export const SIGN_UP_ENDPOINT = "/api/v1/auth/register";
export const SIGN_IN_ENDPOINT = "/api/v1/auth/login";
export const FORGET_PASSWORD_ENDPOINT = "/api/v1/auth/forget-password";
export const RESET_PASSWORD_ENDPOINT = "/api/v1/auth/reset-password";

export const USER_GET_INFO_ENDPOINT = "/api/v1/user/me";
export const USER_UPDATE_INFO_ENDPOINT = "/api/v1/user/me/update";

export const PROJECT_GET_ALL_ENDPOINT = "/api/v1/project/get-all-projects";
export const PROJECT_GET_GET_ONE = "/api/v1/project/get-project?projectId=";
export const PROJECT_GET_BY_TOPIC_ID = "/api/v1/project/get-project-by-topic-id?topicId=";

export const RANKING_GET_ALL_ENDPOINT = "/api/v1/user/rank";

export const TOPIC_GET_ALL_ENDPOINT = "/api/v1/topic/get-all-topics"