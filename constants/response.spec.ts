import { AxiosResponse } from "axios";

export interface ResponsePayload<T> extends AxiosResponse {
  statusCode: number;
  msg: string;
  data: T;
  result: string;
}

export interface AuthPayload {
  accessToken: {
    token: string;
    expire: string;
  };
  refreshToken: {
    token: string;
    expire: string;
  };
}

export interface RegisterPayload {
  address: string;
  email: string;
  hoten: string;
  id: number;
  role: string;
  createdAt: string;
  updatedAt: string;
}

export interface ResetPasswordPayload {
  statusCode: number;
  msg: string;
  type: string | undefined;
}

export interface UserInfoPayload {
  id: number;
  hoten: string;
  sdt: string;
  email: string;
  roleId: 1;
  address: string;
  anhbia: string | null;
  avatar: string | null;
  dateofbirth: string | null;
  noisinh: string | null;
  chucvu: string | null;
  noicongtac: string | null;
  diemso: string | null;
  rank: string | null;
  createdAt: string;
  updatedAt: string;
}

export interface UpdateUserInfoPayload {
  id: number;
  hoten: string;
  sdt: string;
  email: string;
  roleId: 1;
  address: string;
  anhbia: string | null;
  avatar: string | null;
  dateofbirth: string | null;
  noisinh: string | null;
  chucvu: string | null;
  noicongtac: string | null;
  diemso: string | null;
  rank: string | null;
  createdAt: string;
  updatedAt: string;
  isDeleted: boolean;
}

export interface ProjectItem {
  id: number;
  title: string;
  description: string;
  logo: string;
  createdAt: string;
  updatedAt: string;
}

export interface ProjectPayload {
  id: number;
  name: string;
  description: string;
  noiDung: string;
  thumbnail: string;
  topicId: number;
  status: string;
  soTienHuyDong: number;
  soTienDaHuyDong?: number;
  userId?: number;
  projectOwnerId?: number;
  projectOwnerName: string;
  projectOwnerEmail: string;
  projectOwnerPhone: string;
  isPublic: boolean;
  createdAt: string;
  updatedAt: string;
  moneyDonation: MoneyDonationItem[];
}

export interface MoneyDonationItem {
  id: string;
  userId: number;
  projectId: number;
  noteByAdmin: string;
  total: number;
  message?: string;
  createdAt: string;
  updatedAt: string;
  money_donators: MoneyDonatorItem;
}

export interface MoneyDonatorItem {
  id: number;
  hoten: string;
  sdt: string;
  email: string;
  password: string;
  roleId: number;
  address: string;
  anhbia: string | null;
  avatar: string | null;
  dateofbirth: string | null;
  noisinh: string | null;
  chucvu: string | null;
  noicongtac: string | null;
  diemso: string | null;
  rank: string | null;
  createdAt: string;
  updatedAt: string;
}

export interface TopicPayload {
  topicId: number;
  title: string;
  createdAt: string;
  description: string;
  logo: string;
  updatedAt: string;
  TotalProjects: number;
}