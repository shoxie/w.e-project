const animations = {
  pageTransition: {
    initial: {
      opacity: 0,
      x: -100,
    },
    animate: {
      opacity: 1,
      x: 0,
    },
    exit: {
      opacity: 0,
      x: -100,
    },
    transition: {
      type: "spring",
      delay: 2,
    },
  },
  headerTransition: {
    initial: {
      opacity: 0,
      x: -250,
    },
    animate: {
      opacity: 1,
      x: 0,
    },
    transition: {
      type: "spring",
      stiffness: 120,
    },
  },
  modalTransition: {
    initial: {
      opacity: 0,
      y: "-100vh",
      scale: 0.5,
    },
    animate: {
      opacity: 1,
      y: 0,
      scale: 1,
      transition: {
        delay: 0.3,
      },
    },
    exit: {
      opacity: 0,
      y: "100vw",
      transition: {
        delay: 0.3,
      },
    },
    transition: {
      type: "spring",
      stiffness: 500,
    },
  },
  fadeInLeft: {
    initial: {
      opacity: 0,
      x: -100,
      transition: {
        delay: 0.5,
      },
    },
    animate: {
      opacity: 1,
      x: 0,
      transition: {
        delay: 0.5,
      },
    },
    transition: {
      type: "spring",
      stiffness: 120,
      delay: 0.5,
    },
  },
  fadeInUp: {
    initial: {
      opacity: 0,
      translateY: 100,
      transition: {
        delay: 0.5,
      },
    },
    animate: {
      opacity: 1,
      translateY: 0,
      transition: {
        delay: 0.5,
      },
    },
    exit: {
      opacity: 0,
      translateY: 100,
      transition: {
        delay: 0.5,
      }
    },
    transition: {
      type: "spring",
      stiffness: 120,
      delay: 0.5,
    },
  },
  scrollToTopBtn: {
    initial: {
      opacity: 0,
      translateY: 100,
      transition: {
        delay: 0.5,
      },
    },
    animate: {
      opacity: 1,
      translateY: 0,
      transition: {
        delay: 0.5,
      },
    },
    exit: {
      opacity: 0,
      translateY: -500,
      transition: {
        delay: 0.5,
        default : {
          duration: 1
        }
      }
    },
    transition: {
      type: "spring",
      stiffness: 120,
      delay: 0.5,
    },
  },
  stagger: {
    animate: {
      transition: {
        staggerChildren: 0.5,
        duration: 1.5,
        type: 'spring',
        damping: 8,
        mass: 0.4,
      },
    },
  },
};

export default animations;
