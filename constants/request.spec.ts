import { NextRouter } from "next/router";

export interface LoginPayload {
  sdt: string;
  password: string;
}

export interface RegisterPayload {
  hoten: string;
  sdt: string;
  email: string;
  address: string;
  password: string;
}

export interface ForgetPasswordPayload {
  email: string;
}

export interface ResetPasswordPayload {
  newPassword: string;
  token: string;
}
