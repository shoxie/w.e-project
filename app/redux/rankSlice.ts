import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import {
  ResponsePayload,
  UserInfoPayload,
  UpdateUserInfoPayload,
} from "../../constants/response.spec";
import Router from "next/router";
import userService from "../service/user.service";
import { ProjectItem } from "../../constants/response.spec";
import rankingApi from "../api/rankApi";

var initialState: {
  list: UserInfoPayload[];
} = {
  list: [],
};

export const getAllRanking = createAsyncThunk(
  "ranking/getAll",
  async (thunkApi) => {
    // if (payload !== null) {
    // }
    try {
      const response = (await rankingApi.getAll()) as ResponsePayload<
        UserInfoPayload[]
      >;
      switch (response.statusCode) {
        case 200:
          return response;
        case 401:
          toast.error("Hãy thử lại vào lúc khác");
          throw new Error("Credentials expired");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra khi lấy dữ liệu");
      return null;
    }
  }
);

export const rankingSlice = createSlice({
  name: "rankingSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllRanking.fulfilled, (state, action) => {
        console.log('action.payload', action.payload)
      //   return action.payload?.data;
      if (action.payload?.data) {
        state.list = [...action.payload.data];
        return;
      }
    });
  },
});

export default rankingSlice.reducer;
