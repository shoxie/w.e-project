import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  RegisterPayload,
  LoginPayload,
  ForgetPasswordPayload,
  ResetPasswordPayload,
} from "../../constants/request.spec";
import { toast } from "react-toastify";
import {
  ResponsePayload,
  UserInfoPayload,
  UpdateUserInfoPayload,
  TopicPayload,
} from "../../constants/response.spec";
import Router from "next/router";
import userService from "../service/user.service";
import { ProjectItem } from "../../constants/response.spec";
import { UserBasicInfo } from "../../constants/form.spec";
import topicApi from "../api/topicApi";

var initialState: {
  list: TopicPayload[];
} = {
  list: [],
};

export const getAllTopics = createAsyncThunk(
  "topic/getAll",
  async (thunkApi) => {
    // if (payload !== null) {
    // }
    try {
      const response = (await topicApi.getAll()) as ResponsePayload<
        TopicPayload[]
      >;
      switch (response.statusCode) {
        case 200:
          return response;
        case 401:
          toast.error("Hãy đăng nhập lại hoặc thử lại vào lúc khác");
          userService.removeUser();
          throw new Error("Credentials expired");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra khi lấy dữ liệu");
      return null;
    }
  }
);


export const topicSlice = createSlice({
  name: "topicSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllTopics.fulfilled, (state, action) => {
      //   return action.payload?.data;
      if (action.payload?.data) {
        state.list = [...action.payload.data];
        return;
      }
    });
  },
});

export default topicSlice.reducer;
