import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  RegisterPayload,
  LoginPayload,
  ForgetPasswordPayload,
  ResetPasswordPayload,
} from "../../constants/request.spec";
import { toast } from "react-toastify";
import {
  ResponsePayload,
  UserInfoPayload,
  UpdateUserInfoPayload,
} from "../../constants/response.spec";
import Router from "next/router";
import userApi from "../api/userApi";
import userService from "../service/user.service";
import { UserBasicInfo } from "../../constants/form.spec";

var initialState: UserInfoPayload = {
  id: 0,
  hoten: "Nguyen Van A",
  sdt: "123123123",
  email: "user@gmail.com",
  roleId: 1,
  address: "",
  anhbia: null,
  avatar: null,
  dateofbirth: null,
  noisinh: null,
  chucvu: null,
  noicongtac: null,
  diemso: null,
  rank: null,
  createdAt: "",
  updatedAt: "",
};

export const getUserInfo = createAsyncThunk(
  "user/getUserInfo",
  async (thunkApi) => {
    // if (payload !== null) {
    // }
    try {
      const response =
        (await userApi.getUserInfo()) as ResponsePayload<UserInfoPayload>;
      switch (response.statusCode) {
        case 200:
          return response;
        case 401:
          toast.error("Hãy đăng nhập lại hoặc thử lại vào lúc khác");
          throw new Error("Credentials existed");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra");
      userService.removeUser();
      return null;
    }
  }
);

export const updateUserInfo = createAsyncThunk(
  "user/updateUserInfo",
  async (payload: UserBasicInfo, thunkApi) => {
    // if (payload !== null) {
    // }
    try {
      const response = (await userApi.updateUser(
        payload
      )) as ResponsePayload<UpdateUserInfoPayload>;
      switch (response.statusCode) {
        case 200:
          return response;
        case 401:
          console.log("case 401");
          toast.error("Hãy đăng nhập lại hoặc thử lại vào lúc khác")
          throw new Error("Credentials existed");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra");
      return null;
    }
  }
);

export const updateUserAvatar = createAsyncThunk(
  "user/updateUserAvatar",
  async (payload: File, thunkApi) => {
    // if (payload !== null) {
    // }
    try {
      const response = (await userApi.updateUserAvatar(
        payload
      )) as ResponsePayload<UpdateUserInfoPayload>;
      switch (response.statusCode) {
        case 200:
          return response;
        case 401:
          toast.error("Hãy đăng nhập lại hoặc thử lại vào lúc khác")
          throw new Error("Credentials existed");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra");
      return null;
    }
  }
);

export const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getUserInfo.fulfilled, (state, action) => {
      if (action.payload) {
        state = { ...action.payload.data };
        return action.payload.data;
      }
    });
    builder.addCase(getUserInfo.rejected, (state, action) => {
      console.log("rejected", action.payload);
      return state;
    });
    builder.addCase(updateUserInfo.fulfilled, (state, action) => {
      if (action.payload) {
        toast.success("Cập nhật thông tin thành công");
      }
    });
    builder.addCase(updateUserAvatar.fulfilled, (state, action) => {
      if (action.payload) {
        toast.success("Cập nhật ảnh đại diện thành công");
        setTimeout(() => {
          Router.reload()
        }, 3000);
      }
    })
  },
});

export default userSlice.reducer;
