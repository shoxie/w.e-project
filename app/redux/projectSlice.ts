import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  RegisterPayload,
  LoginPayload,
  ForgetPasswordPayload,
  ResetPasswordPayload,
} from "../../constants/request.spec";
import { toast } from "react-toastify";
import {
  ResponsePayload,
  UserInfoPayload,
  UpdateUserInfoPayload,
  ProjectPayload,
} from "../../constants/response.spec";
import Router from "next/router";
import userService from "../service/user.service";
import { ProjectItem } from "../../constants/response.spec";
import { UserBasicInfo } from "../../constants/form.spec";
import projectApi from "../api/projectApi";

var initialState: {
  list: ProjectPayload[];
  currentProject: ProjectPayload | null;
} = {
  list: [],
  currentProject: null
};

export const getAllProjects = createAsyncThunk(
  "project/getAll",
  async (payload: number | null) => {
    // if (payload !== null) {
    // }
    try {
      const res = payload
        ? await projectApi.getByTopicId(payload)
        : await projectApi.getAll();
      const response = res as ResponsePayload<ProjectPayload[]>;

      switch (response.statusCode) {
        case 200:
          return response;
        case 401:
          toast.error("Hãy đăng nhập lại hoặc thử lại vào lúc khác");
          userService.removeUser();
          throw new Error("Credentials expired");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra khi lấy dữ liệu");
      return null;
    }
  }
);

export const getOneProject = createAsyncThunk(
  "project/getOne",
  async (payload: number, thunkApi) => {
    try {
      const response = (await projectApi.getOne(
        payload
      )) as ResponsePayload<ProjectPayload>;
      switch (response.statusCode) {
        case 200:
          return response;
        case 401:
          toast.error("Hãy đăng nhập lại hoặc thử lại vào lúc khác");
          userService.removeUser();
          throw new Error("Credentials expired");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra khi lấy dữ liệu");
      return null;
    }
  }
);

export const projectSlice = createSlice({
  name: "projectSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllProjects.fulfilled, (state, action) => {
      //   return action.payload?.data;
      if (action.payload?.data) {
        state.list = [...action.payload.data];
        return;
      }
    });
    builder.addCase(getOneProject.fulfilled, (state, action) => {
      state.currentProject = {...action.payload?.data } as any
      return;
    })
  },
});

export default projectSlice.reducer;
