import { configureStore } from "@reduxjs/toolkit";
import loadingReducer from "./loadingSlice";
import authReducer from "./authSlice";
import userReducer from "./userSlice";
import projectReducer from "./projectSlice";
import rankReducer from "./rankSlice";
import topicReducer from './topicSlice'
import { UserInfoPayload, ProjectPayload, TopicPayload } from "../../constants/response.spec";

export interface IRootState {
  loading: any;
  auth: any;
  user: UserInfoPayload;
  project: {
    list: ProjectPayload[];
    currentProject: ProjectPayload
  };
  ranking: {
    list: UserInfoPayload[];
  },
  topic: {
    list: TopicPayload[]
  }
}

export const store = configureStore({
  reducer: {
    loading: loadingReducer,
    auth: authReducer,
    user: userReducer,
    project: projectReducer,
    ranking: rankReducer,
    topic: topicReducer
  },
});
