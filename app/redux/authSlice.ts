import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import authApi from "../api/authApi";
import {
  RegisterPayload,
  LoginPayload,
  ForgetPasswordPayload,
  ResetPasswordPayload,
} from "../../constants/request.spec";
import { toast } from "react-toastify";
import { ResponsePayload, AuthPayload } from "../../constants/response.spec";
import Router from "next/router";
import userService from "../service/user.service";
import { startLoading, stopLoading } from "./loadingSlice";

const initialState = {
  isLoading: false,
};

export const signIn = createAsyncThunk(
  "auth/signIn",
  async (payload: LoginPayload, thunkApi) => {
    const { dispatch } = thunkApi;
    dispatch(startLoading());
    // if (payload !== null) {
    // }
    try {
      const response = (await authApi.signIn(
        payload
      )) as ResponsePayload<AuthPayload>;
      switch (response.statusCode) {
        case 200:
          toast.success("Đăng nhập thành công");
          userService.setUser({
            accessToken: {
              token: response.data.accessToken.token,
              expire: response.data.accessToken.expire,
            },
            refreshToken: {
              token: response.data.refreshToken.token,
              expire: response.data.refreshToken.expire,
            },
          });
          dispatch(stopLoading());
          return response;
        case 401:
          toast.error(response.msg);
          throw new Error("Wrong username or password");
        case 404:
          toast.error(response.msg);
          throw new Error("Wrong username or password");
        case 400:
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error: any) {
      console.log(error.response);
      dispatch(stopLoading());
      return null;
    }
  }
);

export const signUp = createAsyncThunk(
  "auth/signUp",
  async (payload: RegisterPayload, thunkApi) => {
    // if (payload !== null) {
    // }
    const {dispatch} = thunkApi
    dispatch(startLoading());
    try {
      const response = (await authApi.signUp(
        payload
      )) as ResponsePayload<RegisterPayload>;
      switch (response.statusCode) {
        case 201:
          toast.success("Đăng ký thành công");
          dispatch(stopLoading());
          return response;
        case 401:
          toast.error(response.msg);
          throw new Error("Credentials existed");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      dispatch(stopLoading());
      throw error;
    }
  }
);

export const forgetPassword = createAsyncThunk(
  "auth/forgetPassword",
  async (payload: ForgetPasswordPayload, thunkApi) => {
    // if (payload !== null) {
    // }
    try {
      const response = (await authApi.forgetPassword(
        payload
      )) as ResponsePayload<ForgetPasswordPayload>;
      switch (response.statusCode) {
        case 200:
          toast.success("Kiểm tra email để có thể cài đặt lại mật khẩu");
          return response;
        case 400:
          toast.error(response.msg);
          throw new Error(response.msg);
        case 401:
          toast.error(response.msg);
          throw new Error(response.msg);
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      return null;
    }
  }
);

export const resetPassword = createAsyncThunk(
  "auth/resetPassword",
  async (payload: ResetPasswordPayload, thunkApi) => {
    // if (payload !== null) {
    // }
    try {
      const response = (await authApi.resetPassword(
        payload
      )) as ResponsePayload<ResetPasswordPayload>;
      switch (response.statusCode) {
        case 200:
          toast.success(
            "Đổi mật khẩu thành công, bạn sẽ được chuyển đến trang đăng nhập"
          );
          Router.push("/login");
          return response;
        case 401:
          toast.error(response.msg);
          throw new Error("Credentials existed");
        case 500:
          toast.error("Có lỗi xảy ra");
          throw new Error("");
        default:
          throw new Error("Error");
      }
    } catch (error) {
      toast.error("Có lỗi xảy ra");
      return null;
    }
  }
);

export const authSlice = createSlice({
  name: "authSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(signIn.fulfilled, (state, action) => {
      if (action.payload) {
        setTimeout(() => {
          Router.push("/");
        }, 3000);
      } else toast.error("Có lỗi xảy ra");
    });
    builder.addCase(signIn.rejected, (state, action) => {
      toast.error("Có lỗi xảy ra");
    });
    builder.addCase(signUp.fulfilled, (state, action) => {
      setTimeout(() => {
        Router.push("/login");
      }, 3000);
    });
    builder.addCase(forgetPassword.fulfilled, (state, action) => {
      Router.push("/");
    });
    builder.addCase(resetPassword.fulfilled, (state, action) => {
      console.log("action", action.payload);
    });
  },
});

export default authSlice.reducer;
