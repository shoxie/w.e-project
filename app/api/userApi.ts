import axiosClient from "../axiosClient";
import {
  USER_GET_INFO_ENDPOINT,
  USER_UPDATE_INFO_ENDPOINT,
} from "../../constants/api";
import { UserBasicInfo } from "../../constants/form.spec";

const userApi = {
  getUserInfo: () => {
    let url = `${USER_GET_INFO_ENDPOINT}`;
    return axiosClient.get(url).then((result) => result);
  },
  updateUser: (user: UserBasicInfo) => {
    let url = `${USER_UPDATE_INFO_ENDPOINT}`;
    return axiosClient.patch(url, user).then((result) => result);
  },
  updateUserAvatar: (avatar: File) => {
    var formData = new FormData();
    formData.append("image", avatar);
    let url = `${USER_UPDATE_INFO_ENDPOINT}`;
    axiosClient.defaults.headers.common["Content-Type"] = "multipart/form-data";
    return axiosClient.patch(url, avatar).then((result) => result);
  },
};

export default userApi;
