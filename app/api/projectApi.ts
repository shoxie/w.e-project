import axiosClient from "../axiosClient";
import {
  PROJECT_GET_ALL_ENDPOINT,
  PROJECT_GET_GET_ONE,
  PROJECT_GET_BY_TOPIC_ID
} from "../../constants/api";
const projectApi = {
  getAll: () => {
    var url = `${PROJECT_GET_ALL_ENDPOINT}`;
    return axiosClient.get(url).then((response) => response);
  },
  getOne: (id: number) => {
    var url = `${PROJECT_GET_GET_ONE}${id}`;
    return axiosClient.get(url).then((response) => {
      return response
    });
  },
  getByTopicId: (id: number) => {
    var url = `${PROJECT_GET_BY_TOPIC_ID}${id}`;
    return axiosClient.get(url).then((response) => {
      console.log("response", response);
      return response
    });
  }
};

export default projectApi;
