import axiosClient from "../axiosClient";
import { RANKING_GET_ALL_ENDPOINT } from "../../constants/api";

const rankingApi = {
  getAll: () => {
    var url = `${RANKING_GET_ALL_ENDPOINT}`;
    return axiosClient.get(url).then((response) => {
        console.log(response);
        return response
    });
  },
};

export default rankingApi;
