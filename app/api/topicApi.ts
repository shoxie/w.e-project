import axiosClient from "../axiosClient";
import { TOPIC_GET_ALL_ENDPOINT } from "../../constants/api";

const topicApi = {
  getAll: () => {
    var url = `${TOPIC_GET_ALL_ENDPOINT}`;
    return axiosClient.get(url).then((response) => {
        console.log("response", response);
        return response
    });
  },
};

export default topicApi;
