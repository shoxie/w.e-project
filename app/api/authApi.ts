import axiosClient from "../axiosClient";
import { SIGN_IN_ENDPOINT, SIGN_UP_ENDPOINT, FORGET_PASSWORD_ENDPOINT, RESET_PASSWORD_ENDPOINT } from "../../constants/api";
import { RegisterPayload, LoginPayload, ForgetPasswordPayload, ResetPasswordPayload } from "../../constants/request.spec";

const authApi = {
  signIn: (credentials: LoginPayload) => {
    let url = `${SIGN_IN_ENDPOINT}`;
    return axiosClient
      .post(url, credentials)
      .then((result) => result);
  },
  signUp: (user: RegisterPayload) => {
    let url = `${SIGN_UP_ENDPOINT}`;
    return axiosClient
      .post(url, user)
      .then((result) => result);
  },
  forgetPassword: (payload: ForgetPasswordPayload) => {
    let url = `${FORGET_PASSWORD_ENDPOINT}`;
    return axiosClient
      .post(url, payload)
      .then((result) => result);
  },
  resetPassword: (payload: ResetPasswordPayload) => {
    let url = `${RESET_PASSWORD_ENDPOINT}`;
    return axiosClient
      .post(url, payload)
      .then((result) => result);
  }
};

export default authApi;
