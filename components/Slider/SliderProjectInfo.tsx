import React, { useState } from "react";
import Link from "next/link";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/navigation";
// import Swiper core and required modules
import SwiperCore, { EffectCoverflow, Navigation } from "swiper";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

SwiperCore.use([Navigation, EffectCoverflow]);
function SliderProjectInfo() {
  const [swiper, setSwiper] = useState<SwiperCore>();

  const handleClick = (e: React.MouseEvent, direction: string) => {
    if (swiper) {
      switch (direction) {
        case "next":
          swiper.slideNext();
          break;
        case "prev":
          swiper.slidePrev();
          break;
        default:
          break;
      }
    }
  };
  return (
    <>
      <div className="relative">
        <Swiper
          onSwiper={(swiper) => setSwiper(swiper)}
          slidesPerGroup={1}
          slidesPerView={2}
          effect={"coverflow"}
          grabCursor={true}
          centeredSlides={true}
          coverflowEffect={{
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
          }}
          // pagination={true}
          centeredSlidesBounds
          className="mySwiper"
          navigation={{
            nextEl: ".next",
            prevEl: ".prev",
          }}
          loop={true}
        >
          {[1, 2, 3, 4, 5, 6].map((item, index) => {
            return (
              <SwiperSlide key={index} className="w-1/2 rounded-md">
                <img
                  src="https://picsum.photos/1920/1080"
                  className="object-cover w-full h-full rounded-md shadow-xl"
                  alt="project"
                />
              </SwiperSlide>
            );
          })}
        </Swiper>
        <div className="absolute top-0 flex flex-row items-center justify-between w-full h-full">
          <button
            className="prev -ml-9 md:-ml-11 relative z-10"
            onClick={(e) => handleClick(e, "prev")}
          >
            <BsChevronLeft className="text-3xl" />
          </button>
          <button
            className="next -mr-9 md:-mr-11 relative z-10"
            onClick={(e) => handleClick(e, "next")}
          >
            <BsChevronRight className="text-3xl" />
          </button>
        </div>
      </div>
    </>
  );
}

export default SliderProjectInfo;
