import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Pagination } from "swiper";
import Image from "next/image";
import Link from "next/link";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import { ProjectPayload } from "../../constants/response.spec";

SwiperCore.use([Pagination]);
function SliderProjectLanding({ projects }: { projects: ProjectPayload[] }) {
  if (projects.length < 3) {
    return (
      <>
        <Swiper
          pagination={{
            clickable: true,
          }}
          className="overflow-hidden mySwiper"
        >
          {projects.map((item, index) => (
            <SwiperSlide key={index} className="overflow-hidden pb-14">
              {projects[0] && (
                <div className="mt-10 overflow-hidden">
                  <div className="flex flex-col gap-10 md:flex-row">
                    <div className="flex-1">
                      <Image
                        src={projects[0].thumbnail}
                        width={750}
                        height={500}
                        alt="project-image"
                        className="rounded-lg"
                      />
                    </div>
                    <div className="flex flex-col items-center justify-center flex-1 gap-5">
                      <p className="text-center text-white">
                        {projects[0].description}
                      </p>
                      <Link href="/projects/info">
                        <a className="px-4 py-2 bg-white shadow-lg text-primary rounded-xl">
                          Tìm hiểu thêm
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
              )}
              {projects[1] && (
                <div className="block mt-10 overflow-hidden">
                  <div className="flex flex-col-reverse gap-10 md:flex-row ">
                    <div className="flex flex-col items-center justify-center flex-1 gap-5">
                      <p className="text-center text-white">
                        {projects[1].description}
                      </p>
                      <Link href="/projects/info">
                        <a className="block px-4 py-2 bg-white shadow-lg text-primary rounded-xl">
                          Tìm hiểu thêm
                        </a>
                      </Link>
                    </div>
                    <div className="flex-1">
                      <Image
                        src={projects[1].thumbnail}
                        width={750}
                        height={500}
                        alt="project-image"
                        className="rounded-lg"
                      />
                    </div>
                  </div>
                </div>
              )}
              {projects[2] && (
                <div className="mt-10 overflow-hidden">
                  <div className="flex flex-col gap-10 md:flex-row ">
                    <div className="flex-1">
                      <Image
                        src={
                          projects[2].thumbnail ??
                          "https://picsum.photos/1920/1080"
                        }
                        width={750}
                        height={500}
                        alt="project-image"
                        className="rounded-lg"
                      />
                    </div>
                    <div className="flex flex-col items-center justify-center flex-1 gap-5">
                      <p className="text-center text-white">
                        {projects[2].description}
                      </p>
                      <Link href="/projects/info">
                        <a className="block px-4 py-2 bg-white shadow-lg text-primary rounded-xl">
                          Tìm hiểu thêm
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
              )}
            </SwiperSlide>
          ))}
        </Swiper>
      </>
    );
  } else
    return (
      <>
        <Swiper
          pagination={{
            clickable: true,
          }}
          className="overflow-hidden mySwiper"
        >
          {Array.from(Array(Math.round(projects.length / 3)).keys()).map(
            (item, index) => (
              <SwiperSlide key={index} className="overflow-hidden pb-14">
                {projects[index * 3] && (
                  <div className="mt-10 overflow-hidden">
                    <div className="flex flex-col gap-10 md:flex-row">
                      <div className="flex-1">
                        <Image
                          src={projects[index * 3].thumbnail}
                          width={750}
                          height={500}
                          alt="project-image"
                          className="rounded-lg"
                        />
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 gap-5">
                        <p className="text-center text-white">
                          {projects[index * 3].description}
                        </p>
                        <Link href="/projects/info">
                          <a className="px-4 py-2 bg-white shadow-lg text-primary rounded-xl">
                            Tìm hiểu thêm
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                )}
                {projects[index * 3 + 1] && (
                  <div className="block mt-10 overflow-hidden">
                    <div className="flex flex-col-reverse gap-10 md:flex-row ">
                      <div className="flex flex-col items-center justify-center flex-1 gap-5">
                        <p className="text-center text-white">
                          {projects[index * 3 + 1].description}
                        </p>
                        <Link href="/projects/info">
                          <a className="block px-4 py-2 bg-white shadow-lg text-primary rounded-xl">
                            Tìm hiểu thêm
                          </a>
                        </Link>
                      </div>
                      <div className="flex-1">
                        <Image
                          src={projects[index * 3 + 1].thumbnail}
                          width={750}
                          height={500}
                          alt="project-image"
                          className="rounded-lg"
                        />
                      </div>
                    </div>
                  </div>
                )}
                {projects[index * 3 + 2] && (
                  <div className="mt-10 overflow-hidden">
                    <div className="flex flex-col gap-10 md:flex-row ">
                      <div className="flex-1">
                        <Image
                          src={
                            projects[index * 3 + 2].thumbnail ??
                            "https://picsum.photos/1920/1080"
                          }
                          width={750}
                          height={500}
                          alt="project-image"
                          className="rounded-lg"
                        />
                      </div>
                      <div className="flex flex-col items-center justify-center flex-1 gap-5">
                        <p className="text-center text-white">
                          {projects[index * 3 + 2].description}
                        </p>
                        <Link href="/projects/info">
                          <a className="block px-4 py-2 bg-white shadow-lg text-primary rounded-xl">
                            Tìm hiểu thêm
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                )}
              </SwiperSlide>
            )
          )}
        </Swiper>
      </>
    );
}

export default SliderProjectLanding;
