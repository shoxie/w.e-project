import React, { useRef, useState } from "react";
import Link from "next/link";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Pagination, Navigation } from "swiper";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import { ProjectPayload } from "../../constants/response.spec";

SwiperCore.use([Pagination, Navigation]);

function SliderProjectItems({ projects }: { projects: ProjectPayload[] }) {
  const [swiper, setSwiper] = useState<SwiperCore>();

  const handleClick = (e: React.MouseEvent, direction: string) => {
    if (swiper) {
      switch (direction) {
        case "next":
          swiper.slideNext();
          break;
        case "prev":
          swiper.slidePrev();
          break;
        default:
          break;
      }
    }
  };
  return (
    <>
      <div className="relative">
        <Swiper
          onSwiper={(swiper) => setSwiper(swiper)}
          breakpoints={{
            300: {
              slidesPerView: 1,
              slidesPerGroup: 1,
            },
            898: {
              slidesPerView: 2,
              slidesPerGroup: 2,
            },
          }}
          loop={true}
          loopFillGroupWithBlank={true}
          pagination={{
            clickable: true,
          }}
          className="mySwiper"
        >
          {projects.map((item, index) => {
            return (
              <SwiperSlide key={index} className="px-5 pb-14 md:px-14">
                <div className="overflow-hidden rounded-md shadow-lg">
                  <div>
                    <img
                      src={item.thumbnail ?? "https://picsum.photos/1920/1080"}
                      className="object-cover w-full h-full rounded-t-md"
                      alt="project"
                    />
                  </div>
                  <div className="flex flex-col items-center justify-center gap-5 px-5 py-10 bg-white border rounded-b-md ">
                    <h4 className="text-2xl font-semibold">
                      Dự án {item.name}
                    </h4>
                    <p className="text-center">{item.description}</p>
                    <Link href={`/projects/${item.id}`}>
                      <button className="px-4 py-2 text-white shadow-lg bg-primary rounded-xl">
                        Tìm hiểu thêm
                      </button>
                    </Link>
                  </div>
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
        <div className="absolute top-0 flex flex-row items-center justify-between w-full h-full">
          <button
            className="relative z-10 -ml-3 prev md:-ml-11"
            onClick={(e) => handleClick(e, "prev")}
          >
            <BsChevronLeft className="text-3xl" />
          </button>
          <button
            className="relative z-10 -mr-3 next md:-mr-11"
            onClick={(e) => handleClick(e, "next")}
          >
            <BsChevronRight className="text-3xl" />
          </button>
        </div>
      </div>
    </>
  );
}

export default SliderProjectItems;
