import type { NextPage } from "next";
import dynamic from "next/dynamic";
const Header = dynamic(() => import("../Home/Header"));
const HomeFooter = dynamic(() => import("../Home/Footer"), {
  ssr: false,
});
const ScrollTop = dynamic(() => import("../../Icon/ScrollTop"), {
  ssr: false,
});

const BlogLayout: NextPage = ({ children }) => {
  return (
    <>
      <Header />
      <div className="pt-16">
        <main className="">{children}</main>
      </div>
      <HomeFooter />
      <ScrollTop />
    </>
  );
};

export default BlogLayout;
