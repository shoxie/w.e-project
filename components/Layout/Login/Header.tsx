import Image from "next/image";
import Link from "next/link";
import { Menu, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import { GiHamburgerMenu } from "react-icons/gi";

var navPath = [
  {
    name: "Hỗ trợ",
    path: "/",
  },
  {
    name: "Điều khoản dịch vụ",
    path: "/projects",
  },
  {
    name: "Chính sách bảo mật",
    path: "/blogs",
  },
];

function DropdownMenu() {
  return (
    <div className="w-56 text-right">
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button className="pr-5">
            <GiHamburgerMenu
              className="w-5 h-5 ml-2 -mr-1 text-violet-200 hover:text-violet-100"
              aria-hidden="true"
            />
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute w-56 mt-2 overflow-hidden origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg right-1/2 ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div>
              {navPath.map((item, idx) => (
                <Menu.Item key={idx}>
                  {({ active }) => (
                    <Link href="">
                      <a
                        className={`${
                          active
                            ? "bg-tertiary-700 text-primary"
                            : "text-gray-900"
                        } group flex items-center w-full px-5 py-3 text-md transition-colors`}
                      >
                        {item.name}
                      </a>
                    </Link>
                  )}
                </Menu.Item>
              ))}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
}

const Header = () => {
  return (
    <header className="absolute top-0 left-0 right-0 z-10 w-full bg-white border-b text-md">
      <nav className="flex items-center justify-between p-3 mx-auto md:px-16 md:py-4 max-w-screen-xxl">
        <div>
          <Link href="/">
            <a>
              <Image
                src="/image/login/logo.jpg"
                alt="logo"
                width="50"
                height="50"
              />
            </a>
          </Link>
        </div>
        <div className="hidden md:block">
          <ul className="flex items-center justify-around space-x-2 md:space-x-7">
            <li>
              <Link href="">
                <a>Hỗ Trợ</a>
              </Link>
            </li>
            <li>
              <Link href="">
                <a>Điều Khoản Dịch Vụ</a>
              </Link>
            </li>
            <li>
              <Link href="">
                <a>Chính Sách Bảo Mật</a>
              </Link>
            </li>
          </ul>
        </div>
        <div className="block md:hidden">
          <DropdownMenu />
        </div>
      </nav>
    </header>
  );
};

export default Header;
