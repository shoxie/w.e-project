import type { NextPage } from "next";
import dynamic from "next/dynamic";
const Header = dynamic(() => import("./Header"));
import { ToastContainer } from "react-toastify";

const LoginLayout: NextPage = ({ children }) => {
  return (
    <>
      <Header />
      {/* <ToastContainer /> */}
      <main className="mx-auto max-w-screen-xxl">{children}</main>
    </>
  );
};

export default LoginLayout;
