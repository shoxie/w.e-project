import React, {
  Fragment,
  useEffect,
  useState,
  useCallback,
  MouseEvent,
} from "react";
import { AiOutlineBars, AiFillEdit } from "react-icons/ai";
import Link from "next/link";
import { IoBookOutline, IoHeartOutline } from "react-icons/io5";
import { Menu, Transition } from "@headlessui/react";
import { FiChevronRight } from "react-icons/fi";

var projects = [
  {
    name: "Phát triển du lịch trải nghiệm cộng đồng",
    path: "/projects/info",
  },
  {
    name: "Tri thức nhân loại - Chinh phục tương lai",
    path: "/projects/info",
  },
  {
    name: "Suối hồng ấm nồng yêu thương",
    path: "/projects/info",
  },
  {
    name: "Nghĩa tình sâu nặng",
    path: "/projects/info",
  },
  {
    name: "Giúp măng mọc thẳng",
    path: "/projects/info",
  },
  {
    name: "Ấm áp vòng tay",
    path: "/projects/info",
  },
];

const ListBox = () => {
  const [clicked, setClicked] = useState(false);

  function generateClass(index: number) {
    var base =
      "flex flex-row items-center min-w-full px-5 py-3 text-lg transition-all duration-500 md:space-x-3 hover:text-primary group hover:bg-gray-200";
    if (index === 0) return base + " rounded-t-xl";
    if (index === projects.length) return base + " rounded-b-xl";
    else return base;
  }

  function handleClick() {
    setClicked(!clicked);
  }
  const handleUserKeyPress = useCallback((event) => {
    if (event.target.id != "menu-button") {
      setClicked(false);
    }
  }, []);

  useEffect(() => {
    window.addEventListener("click", handleUserKeyPress);
    return () => {
      window.removeEventListener("click", handleUserKeyPress);
    };
  }, [handleUserKeyPress]);

  return (
    <div>
      <Menu as="div" className="relative text-left">
        <div className="">
          <Menu.Button className="w-full">
            <div
              onClick={handleClick}
              className={
                clicked
                  ? "flex flex-row items-center justify-center min-w-full px-3 py-3 text-lg transition-all duration-500 space-x-3 text-primary group bg-gray-200"
                  : "flex flex-row items-center justify-center min-w-full px-3 py-3 text-lg transition-all duration-500 space-x-3 hover:text-primary group hover:bg-gray-200"
              }
            >
              <AiOutlineBars className="mr-3" />
              <span>
                <a>Các chủ đề thiện nguyện</a>
              </span>
              <div
                className={
                  clicked
                    ? "opacity-100"
                    : "opacity-0 group-hover:opacity-100 transition-all"
                }
              >
                <FiChevronRight />
              </div>
            </div>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute md:right-64 xxl:left-[19rem] font-normal z-10 w-full overflow-hidden origin-bottom-left bg-white divide-y divide-gray-100 shadow-lg md:-top-2 md:w-72 md:mt-2 rounded-xl md:origin-bottom-left ring-1 ring-black ring-opacity-5 focus:outline-none">
            {projects.map((project, index) => {
              return (
                <Menu.Item key={index}>
                  <div className={generateClass(index)}>
                    <Link href={project.path}>
                      <a>{project.name}</a>
                    </Link>
                  </div>
                </Menu.Item>
              );
            })}
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
};

export default function ProjectSidebar({
  setMenuShown,
}: {
  setMenuShown: (state: boolean) => void;
}) {
  return (
    <div className="relative z-50 flex items-center justify-center md:block max-w-max">
      <div className="flex flex-col bg-white shadow-md max-w-max md:w-full rounded-xl">
        <div className="flex flex-row items-center px-3 py-3 space-x-3 text-lg transition-all duration-500 hover:text-primary rounded-t-xl group hover:bg-gray-200">
          <IoHeartOutline className="mr-3" />
          <Link href="/projects">
            <a className="font-normal text-center">Tổng quan</a>
          </Link>
        </div>
        {/* <div className="flex flex-row items-center justify-center">
          <ListBox />
        </div> */}
        <div className="flex flex-row items-center px-3 py-3 space-x-3 text-lg transition-all duration-500 hover:text-primary rounded-b-xl group hover:bg-gray-200">
          <IoBookOutline className="mr-3" />
          <Link href="/projects/instruction">
            <a className="font-normal text-center">Hướng dẫn đóng góp</a>
          </Link>
        </div>
      </div>
    </div>
  );
}
