import type { NextPage } from "next";
import { RiHandHeartLine } from "react-icons/ri";
import { AiOutlineHeart } from "react-icons/ai";
import { motion } from "framer-motion";
import animations from "../../../constants/animation";
import dynamic from "next/dynamic";
import { useSelector } from "react-redux";
import { IRootState } from "../../../app/redux/store";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { getUserInfo } from "../../../app/redux/userSlice";
import { useState } from "react";

const HomeHeader = dynamic(
  () => import("../../../components/Layout/Home/Header"),
  { ssr: false }
);
const Avatar = dynamic(() => import("../../User/Avatar"), {
  ssr: false,
});

const AchievementLayout: NextPage = ({ children }) => {
  const dispatch = useDispatch();
  const userData = useSelector((state: IRootState) => state.user);

  const [showModalAva, setShowModalAva] = useState(false);
  const [showModalCover, setShowModalCover] = useState(false);

  const handleShowAva = () => {
    setShowModalAva(true);
  };

  const handleHideAva = () => {
    setShowModalAva(false);
  };

  const handleShowCover = () => {
    setShowModalCover(true);
  };

  const handleHideCover = () => {
    setShowModalCover(false);
  };

  useEffect(() => {
    dispatch(getUserInfo());
  }, []);

  return (
    <>
      <HomeHeader />
      <motion.div
        variants={animations.pageTransition}
        initial="initial"
        animate="animate"
        exit="exit"
        className="px-0 md:px-16"
      >
        <div className="flex flex-col items-center pt-24">
          {/*profile cover*/}
          <div className="relative w-full px-5 overflow-hidden rounded-xl h-72 md:shadow-xl md:px-0">
            <img
              src="https://picsum.photos/1920/1080"
              className="object-cover w-full h-full cursor-pointer rounded-xl"
              alt="profile-cover"
            />
          </div>
          <div className="flex flex-col w-full px-10 -mt-16 items-flex-start md:-mt-36">
            <div className="flex flex-col pt-5 items-flex-start md:justify-start md:items-start md:w-auto md:space-x-5 md:pt-0">
              {/*user profile picture */}
              <div className="relative flex items-center justify-center">
                <img
                  src="https://picsum.photos/1920/1080"
                  className="w-32 h-32 border-4 border-white rounded-full md:w-64 md:h-64"
                  alt="profile-picture"
                />
              </div>
              {/*user name and points */}
              <div className="flex flex-col items-center pb-8 mt-4 space-y-3">
                <div className="">
                  <span className="text-4xl font-bold">John Barton</span>
                </div>
                <div className="flex items-center space-x-2 font-semibold text-primary">
                  <RiHandHeartLine className="text-2xl" />
                  <span>{userData.diemso ?? 0} points</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="grid items-start w-full grid-cols-1 px-5 pb-24 space-y-10 md:grid-cols-12 md:space-x-10 md:space-y-0 md:px-0">
          <div className="flex w-full bg-white justify-center md:items-center md:col-span-4">
            <div className="p-10 border rounded-lg shadow-lg ">
              <div className="flex justify-center w-full mb-4 space-x-5">
                {[1, 2, 3, 4, 5].map((item) => (
                  <div key={item}>
                    <AiOutlineHeart className="text-3xl text-primary" />
                  </div>
                ))}
              </div>
              <div className="flex items-center justify-center space-x-2 text-primary mb-6">
                <RiHandHeartLine className="text-2xl" />
                <span>{userData.diemso ?? 0} points</span>
              </div>
              <div className="flex flex-col items-center w-full space-y-5">
                {[1, 2, 3, 4, 5].map((item) => (
                  <div key={item}>Lorem ipsum dolor sit amet</div>
                ))}
              </div>
            </div>
          </div>
          <div className="md:col-span-8">{children}</div>
        </div>
      </motion.div>
    </>
  );
};

export default AchievementLayout;
