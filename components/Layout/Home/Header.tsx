import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
const DropdownMenu = dynamic(() => import("./DropdownMenu"), { ssr: false });
const ScrollPercent = dynamic(() => import("./ScrollPercent"), { ssr: false });
import userService from "../../../app/service/user.service";
import { AuthPayload } from "../../../constants/response.spec";
import { useEffect, useState } from "react";
import DropdownProfile from "./DropdownProfile";
import ProjectSidebar from "../Project/Sidebar";

var navPath = [
  {
    name: "Trang chủ",
    path: "/",
  },
  {
    name: "Dự án",
    path: "/projects",
  },
  {
    name: "Blog",
    path: "/blogs",
  },
  {
    name: "Bảng xếp hạng",
    path: "/ranking",
  },
];

var protectedRoutes = ["/users"];

const HomeHeader = () => {
  const router = useRouter();
  const [user, setUser] = useState<AuthPayload | null>(null);
  const [isShown, setIsShown] = useState(false);
  const [menuShown, setMenuShown] = useState(false);

  useEffect(() => {
    var user = userService.getUser();
    if (protectedRoutes.includes(router.pathname)) {
      if (!user) {
        router.push("/login");
      } else setUser(user);
    } else setUser(user);
  }, []);

  // const projectTab = navPath[1].path;

  const handleHoverEnterProjectTab = () => {
    setIsShown(true);
  };

  // handleHoverProjectTab();

  return (
    <>
      <header className="fixed top-0 z-50 w-full bg-white border-b text-md">
        <nav className="flex items-center justify-between p-3 mx-auto md:px-16 md:py-4 max-w-screen-xxl">
          <div>
            <Link href="/">
              <a className="flex items-center ">
                <Image
                  src="/image/login/logo.jpg"
                  alt="logo"
                  width="50"
                  height="50"
                />
              </a>
            </Link>
          </div>
          <div className="flex items-center justify-center flex-1 md:hidden">
            {navPath.map((item) => {
              if (item.path === router.pathname) {
                return (
                  <div key={item.name} className="text-xl text-primary">
                    {item.name}
                  </div>
                );
              }
            })}
          </div>
          <ul className="relative items-center justify-center hidden font-medium md:flex md:space-x-8 md:ml-20">
            {navPath.map((navItem, idx) => (
              <div key={idx}>
                {navItem.name === "Dự án" ? (
                  <div
                    className="cursor-pointer"
                    onClick={() => setIsShown(!isShown)}
                    onMouseOver={() => setIsShown(true)}
                    onMouseEnter={() => setIsShown(true)}
                  >
                    <a
                      className={`py-2 rounded-full transition-colors relative  ${
                        navItem.path === router.pathname
                          ? `text-primary nav-link`
                          : ""
                      }`}
                    >
                      {navItem.name}
                    </a>
                  </div>
                ) : (
                  <Link href={navItem.path}>
                    <a
                      className={`py-2 rounded-full transition-colors relative  ${
                        navItem.path === router.pathname
                          ? `text-primary nav-link`
                          : ""
                      }`}
                    >
                      {navItem.name}
                    </a>
                  </Link>
                )}
                {isShown && (
                  <div
                    className="absolute origin-bottom-left md:top-7 xs:right-12 md:left-0"
                    onClick={() => setIsShown(true)}
                    onMouseLeave={() => setIsShown(false)}
                  >
                    <ProjectSidebar setMenuShown={setMenuShown} />
                  </div>
                )}
              </div>
            ))}
          </ul>
          {user ? (
            <DropdownProfile
              hasUser={true}
              menuShown={menuShown}
              setMenuShown={setMenuShown}
            />
          ) : (
            <>
              <ul className="items-center justify-around hidden font-bold md:flex space-x-7">
                <li>
                  <Link href="/register">
                    <a>Đăng ký</a>
                  </Link>
                </li>
                <li>
                  <Link href="/login">
                    <a>Đăng nhập</a>
                  </Link>
                </li>
              </ul>
              <div className="block min-h-full md:hidden">
                <DropdownMenu
                  menuShown={menuShown}
                  setMenuShown={setMenuShown}
                />
              </div>
            </>
          )}
        </nav>
        <ScrollPercent />
      </header>
    </>
  );
};

export default HomeHeader;
