import { Menu, Transition } from "@headlessui/react";
import { Fragment, useEffect } from "react";
import { BiUserCircle, BiLogOut } from "react-icons/bi";
import { AiOutlineSetting } from "react-icons/ai";
import { GiHamburgerMenu } from "react-icons/gi";
import Cookies from "js-cookie";
import Router from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../../app/redux/store";
import { getUserInfo } from "../../../app/redux/userSlice";
import userService from "../../../app/service/user.service";
import Link from "next/link";

export interface navItem {
  name: string;
  path: string;
  icon?: JSX.Element;
  action?: () => void;
  isSplitPoint?: boolean;
  userRequired?: boolean;
}

var navPath: navItem[] = [
  {
    name: "Trang chủ",
    path: "/",
    userRequired: false,
    action: () => {
      Router.push("/");
    },
  },
  {
    name: "Dự án",
    path: "/projects",
    userRequired: false,
    action: () => {
      Router.push("/projects");
    },
  },
  {
    name: "Blog",
    path: "/blogs",
  },
  {
    name: "Bảng xếp hạng",
    path: "/ranking",
    isSplitPoint: true,
  },
  {
    icon: <BiUserCircle className="mr-3 text-lg" />,
    name: "Trang cá nhân",
    path: "/user",
    userRequired: true,
    action: () => {
      Router.push("/user");
    },
  },
  {
    icon: <AiOutlineSetting className="mr-3 text-lg" />,
    name: "Cài đặt",
    path: "/setting",
  },
  {
    icon: <BiLogOut className="mr-3 text-lg" />,
    name: "Đăng xuất",
    path: "/login",
    userRequired: true,
    action: () => {
      Cookies.remove("user");
      Router.push("/login");
    },
  },
];

function DropdownProfile({
  hasUser,
  menuShown,
  setMenuShown,
}: {
  hasUser: boolean;
  menuShown: boolean;
  setMenuShown: (state: boolean) => void;
}) {
  const dispatch = useDispatch();
  const userData = useSelector((state: IRootState) => state.user);

  useEffect(() => {
    var user = userService.getUser();
    if (user) dispatch(getUserInfo());
  }, []);
  return (
    <div className="text-right relative z-50">
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button className="pr-5">
            {userData.avatar ? (
              <img
                src={userData.avatar}
                className="w-12 h-12 rounded-full"
                alt="avatar"
              />
            ) : (
              <GiHamburgerMenu
                className="w-5 h-5 ml-2 -mr-1 text-violet-200 hover:text-violet-100"
                aria-hidden="true"
              />
            )}
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 w-56 mt-2 origin-top-right bg-white divide-y divide-gray-100 shadow-lg rounded-xl ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div>
              {navPath.map((item, idx) => {
                if (item.userRequired && !hasUser) return null;
                return (
                  <Menu.Item key={idx}>
                    {({ active }) => (
                      <Link href={item.path}>
                        <a
                          className={`${
                            active
                              ? "bg-tertiary-700 text-primary"
                              : "text-gray-900"
                          } group flex rounded-md items-center w-full px-5 py-3 text-md transition-colors hover:bg-tertiary-700 hover:text-primary ${
                            item.isSplitPoint ? "border-b pb-2" : ""
                          }`}
                          onClick={item.action ? item.action : () => {}}
                        >
                          {item.icon && item.icon}
                          {item.name}
                        </a>
                      </Link>
                    )}
                  </Menu.Item>
                );
              })}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
}

export default DropdownProfile;
