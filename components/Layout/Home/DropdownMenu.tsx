import { Menu, Transition } from "@headlessui/react";
import Link from "next/link";
import { Fragment } from "react";
import { GiHamburgerMenu } from "react-icons/gi";
import ProjectSidebar from "../Project/Sidebar";

var navPath = [
  {
    name: "Trang chủ",
    path: "/",
  },
  {
    name: "Dự án",
    path: "/projects",
  },
  {
    name: "Blog",
    path: "/blogs",
  },
  {
    name: "Bảng xếp hạng",
    path: "/ranking",
  },
  {
    name: "Đăng nhập",
    path: "/login",
  },
  {
    name: "Đăng Ký",
    path: "/register",
  },
];

function DropdownMenu({
  menuShown,
  setMenuShown,
}: {
  menuShown: boolean;
  setMenuShown: (state: boolean) => void;
}) {
  return (
    <div className="text-right relative z-50">
      <Menu as="div" className="relative inline-block text-left w-full">
        <div className="flex items-center ">
          <Menu.Button className="pr-5 flex items-center">
            <GiHamburgerMenu
              className="w-5 h-5 ml-2 -mr-1 text-violet-200 hover:text-violet-100"
              aria-hidden="true"
            />
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute w-56 mt-2 overflow-hidden origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg right-1/2 ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div>
              {navPath.map((item) => (
                <Menu.Item key={item.name}>
                  {({ active }) =>
                    item.name === "Dự án" ? (
                      <div>
                        <a
                          className={`${
                            active
                              ? "bg-tertiary-700 text-primary"
                              : "text-gray-900"
                          } group flex items-center w-full px-5 py-3 text-md transition-colors hover:bg-tertiary-700 hover:text-primary`}
                          onClick={() => {
                            if (item.name === "Dự án") {
                              setMenuShown(!menuShown);
                            }
                          }}
                        >
                          {item.name}
                        </a>
                      </div>
                    ) : (
                      <Link href={item.path}>
                        <a
                          className={`${
                            active
                              ? "bg-tertiary-700 text-primary"
                              : "text-gray-900"
                          } group flex items-center w-full px-5 py-3 text-md transition-colors hover:bg-tertiary-700 hover:text-primary`}
                        >
                          {item.name}
                        </a>
                      </Link>
                    )
                  }
                </Menu.Item>
              ))}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
      {menuShown && (
        <div
          className="absolute right-0 top-10 z-50 min-w-[90vw]"
          onClick={() => setMenuShown(true)}
          onMouseLeave={() => setMenuShown(false)}
        >
          <ProjectSidebar setMenuShown={setMenuShown} />
        </div>
      )}
    </div>
  );
}

export default DropdownMenu;
