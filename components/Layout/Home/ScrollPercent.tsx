import { NextPage } from "next";
import { useState, useEffect } from "react";

export default function ScrollPercent() {
  const [scrollPercent, setScrollPercent] = useState(0);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  function handleScroll() {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    const scrollHeight =
      document.documentElement.scrollHeight || document.body.scrollHeight;
    const clientHeight =
      document.documentElement.clientHeight || document.body.clientHeight;
    const scrollPercent = (scrollTop / (scrollHeight - clientHeight)) * 100;
    setScrollPercent(scrollPercent);
  }

  return (
    <div
      className={`bg-primary h-1 rounded-full absolute top-full left-0`}
      style={{ width: `${scrollPercent}%` }}
    ></div>
  );
}
