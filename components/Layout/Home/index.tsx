import type { NextPage } from "next";
import dynamic from "next/dynamic";
const HomeHeader = dynamic(() => import("./Header"), {
  ssr: false,
});
const HomeFooter = dynamic(() => import("./Footer"), {
  ssr: false,
});
const ScrollTop = dynamic(() => import("../../Icon/ScrollTop"), {
  ssr: false,
});
// import { ToastContainer, toast } from "react-toastify";

const HomeLayout: NextPage = ({ children }) => {
  return (
    <div>
      <HomeHeader />
      <main>{children}</main>
      <HomeFooter />
      <ScrollTop />
    </div>
  );
};

export default HomeLayout;
