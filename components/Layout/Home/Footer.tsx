import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

var navPath = [
  {
    name: "Trang chủ",
    path: "/",
  },
  {
    name: "Dự án",
    path: "/projects",
  },
  {
    name: "Blog",
    path: "/blogs",
  },
  {
    name: "Bảng xếp hạng",
    path: "/ranking",
  },
];

const HomeFooter = () => {
  const router = useRouter();

  return (
    <>
      <footer className="bg-white py-5 border-t text-center md:text-left">
        <div className="px-10 flex flex-col md:flex-row items-center md:items-start justify-between gap-5 md:gap-3 mx-auto max-w-screen-xxl">
          <div className="flex-1">
            <h3 className="text-2xl font-semibold">Sáng lập</h3>
            <div className="mt-2 text-lg">
              <span className="font-semibold">
                Sáng lập & pháp nhân chính thức:{" "}
              </span>
              Chương trình công tác xã hội lực lượng vũ trang Phan Trọng Bình.{" "}
            </div>
          </div>
          <div className="flex-1">
            <h3 className="text-2xl font-semibold">Liên hệ</h3>
            <div className="md:mt-2 text-lg">
              <span className="font-semibold">Địa chỉ: </span> 91 Nguyễn Văn Thủ
              - Phường Đa Kao - Quận 1 - TPHCM.
            </div>
            <div className="md:mt-2 text-lg">
              <span className="font-semibold">Website: </span>{" "}
              www.ahllvtndphantrongbinh.vn
            </div>
            <div className="md:mt-2 text-lg">
              <span className="font-semibold">Email: </span>
              bandieuhanh@ahllvtndphantrongbinh.vn
            </div>
            <div className="md:mt-2 text-lg">
              <a
                href="https://www.facebook.com/quyctxhahllvtndphantrongbinh"
                className="no-underline"
              >
                <span className="font-semibold">Fanpage: </span>Chương trình
                CTXH ANH HÙNG LỰC LƯỢNG VŨ TRANG NHÂN DÂN PHAN TRỌNG BÌNH
              </a>
            </div>
          </div>
          <div className="flex-1">
            <h3 className="text-2xl font-semibold">Đại diện</h3>
            <div className="md:mt-2 text-lg">
              <span className="font-semibold">Bà Phạm Thị Bút </span>- Chủ tịch
              Hội đồng Sáng lập Chương trình Công tác xã hội Anh hùng Lực lượng
              vũ trang nhân dân Phan Trọng Bình.
            </div>
            <div className="md:mt-2 text-lg">
              <span className="font-semibold">Điện thoại: </span>
              0933996568
            </div>
            <div className="mt-4 text-lg">
              <span className="font-semibold">Bà Phan Hương Giang </span>- Chủ
              tịch - Trưởng Ban điều hành Chương trình Công tác xã hội Anh hùng
              Lực lượng vũ trang nhân dân Phan Trọng Bình
            </div>
            <div className="md:mt-2 text-lg">
              <span className="font-semibold">Điện thoại: </span>
              0903326586
            </div>
          </div>
        </div>
        <div className="px-10 mt-8 md:mt-16 flex flex-col md:flex-row items-center justify-between gap-10 mx-auto max-w-screen-xxl">
          <div>
            <Image
              src="/image/project/logo-footer.jpg"
              width={400}
              height={80}
              alt="logo-footer"
            />
          </div>
          <div className="flex flex-col tablet-sm:flex-row items-center justify-center gap-5">
            {navPath.map((item, idx) => {
              return (
                <div key={idx}>
                  <Link href={item.path}>
                    <a className="uppercase text-primary text-md lg:text-lg">
                      {item.name}
                    </a>
                  </Link>
                </div>
              );
            })}
          </div>
        </div>
        <hr className="mt-2" />
        <div className="px-10 mt-4 text-lg text-left mx-auto max-w-screen-xxl">
          All right reserves 20xx
        </div>
      </footer>
    </>
  );
};

export default HomeFooter;
