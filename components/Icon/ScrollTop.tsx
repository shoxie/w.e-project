import { useState, useEffect } from "react";
import { IoIosArrowUp } from "react-icons/io";
import { motion, AnimatePresence } from "framer-motion";
import animations from "../../constants/animation";

function ScrollTopIcon() {
  const [show, setShow] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      window.scrollY >= 30 ? setShow(true) : setShow(false);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  return (
    <AnimatePresence exitBeforeEnter>
      {show && (
        <motion.div
          variants={animations.scrollToTopBtn}
          initial="initial"
          animate="animate"
          exit="exit"
          className="fixed z-10 bottom-10 right-10"
        >
          <a
            className="block p-4 text-white rounded-full cursor-pointer bg-primary shadow-lg"
            onClick={scrollTop}
          >
            <IoIosArrowUp />
          </a>
        </motion.div>
      )}
    </AnimatePresence>
  );
}

export default ScrollTopIcon;
