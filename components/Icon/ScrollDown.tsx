import { BsChevronDown } from "react-icons/bs";
import { useEffect, useState } from "react";

const ScrollDownIcon = () => {
  const [scrollPercent, setScrollPercent] = useState(0);
  const [opacity, setOpacity] = useState(0);
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    setOpacity(scrollPercent);
  }, [scrollPercent]);
  function handleScroll() {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    const scrollHeight =
      document.documentElement.scrollHeight || document.body.scrollHeight;
    const clientHeight =
      document.documentElement.clientHeight || document.body.clientHeight;
    const scrollPercent = (scrollTop / (scrollHeight - clientHeight)) * 100;
    setScrollPercent(scrollPercent);
  }

  const scrollDown = () => {
    window.scrollTo({ top: 1000, behavior: "smooth" });
  };

  return (
    <div className="flex flex-col items-center space-y-3" onClick={scrollDown}>
      <div>
        <span className="capitalize text-primary">Scroll down to discover</span>
      </div>
      <div className="flex flex-col items-center justify-center w-full space-y-8">
        {[1, 2, 3, 4, 5].map((i) => (
          <div
            key={i}
            className={`w-2 h-2 rounded-full bg-primary`}
            style={{
              backgroundColor: "#00c2ff",
              opacity: `${(i * 2 * 10) / 100 - scrollPercent / 100}`,
            }}
          />
        ))}
      </div>
      <div>
        <button onClick={scrollDown}>
          <BsChevronDown className="text-5xl text-primary" />
        </button>
      </div>
    </div>
  );
};

export default ScrollDownIcon;
