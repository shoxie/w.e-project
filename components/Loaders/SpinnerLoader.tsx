import { AiOutlineLoading } from "react-icons/ai";
export default function SpinnerLoader() {
  return (
    <div className="flex flex-row items-center justify-center min-h-full space-x-3 text-white">
      <AiOutlineLoading className="animate-spin" />
      <span className="text-lg">Đang chờ</span>
    </div>
  );
}
