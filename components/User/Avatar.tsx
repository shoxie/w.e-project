import React from "react";

export default function Avatar({
  className,
  image,
  rank,
  handleUpload,
}: {
  className: string;
  image: string;
  rank: number;
  handleUpload?: () => void;
}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 252 226"
      className={className}
    >
      <defs>
        <linearGradient
          id="linear-gradient"
          x1="24"
          y1="150"
          x2={`${rank * 100}`}
          y2="150"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stopColor="#00c2ff" />
          <stop offset="0.23" stopColor="#4dbfe3" />
          <stop offset="0.42" stopColor="#89bdcd" />
          <stop offset="0.56" stopColor="#adbcc0" />
          <stop offset="0.63" stopColor="#bbb" />
        </linearGradient>
      </defs>
      <image
        clipPath="url(#clip)"
        width="512"
        height="512"
        x="-50"
        y="-50"
        xlinkHref={image}
        className="object-fill overflow-visible align-middle w-96 h-80"
      />
      <defs>
        <clipPath id="clip" clipRule="nonzero" clipPath=" ">
          <path
            fill="#c4c4c4"
            d="M226.82,128.87A97.82,97.82,0,1,1,129,31.05,97.82,97.82,0,0,1,226.82,128.87Z"
            transform="translate(-0.5 -0.5)"
          />
        </clipPath>
      </defs>

      <circle
        fill="#00c2ff"
        cx="191.8"
        cy="198.25"
        r="18.3"
        className="hover:cursor-pointer"
        onClick={handleUpload ? handleUpload : () => {}}
      />
      <path
        fill="#fff"
        d="M204.26,193.89V207a2.18,2.18,0,0,1-2.18,2.17H183a2.17,2.17,0,0,1-2.17-2.17V193.89a2.17,2.17,0,0,1,2.17-2.17h4l.57-1.49a2.24,2.24,0,0,1,2.06-1.43h5.72a2.22,2.22,0,0,1,2.06,1.43l.57,1.49h4A2.12,2.12,0,0,1,204.26,193.89ZM198,200.46a5.49,5.49,0,1,0-5.49,5.49A5.49,5.49,0,0,0,198,200.46Zm-1.43,0a4,4,0,1,1-4-4A4,4,0,0,1,196.59,200.46Z"
        transform="translate(-0.5 -0.5)"
        className="hover:cursor-pointer"
        onClick={handleUpload ? handleUpload : () => {}}
      />
      <path
        fill="none"
        strokeLinecap="round"
        strokeMiterlimit="10px"
        strokeWidth="10px"
        strokeDasharray="30 28.4"
        stroke="url(#linear-gradient)"
        d="M29,210.5a121,121,0,0,1,242,0"
        transform="translate(-24 -84.5)"
      >
        <title>Bạn cần 100 kinh nghiệm nữa để lên hạng</title>
      </path>
    </svg>
  );
}
