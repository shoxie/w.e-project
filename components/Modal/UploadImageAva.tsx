import Image from "next/image";
import animations from "../../constants/animation";
import { motion, AnimatePresence } from "framer-motion";
import { updateUserAvatar } from "../../app/redux/userSlice";
import { useDispatch } from "react-redux";
import { useState } from "react";
import axios from "axios";
import { USER_UPDATE_INFO_ENDPOINT } from "../../constants/api";
import userService from "../../app/service/user.service";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import dynamic from "next/dynamic";
const SpinnerLoader = dynamic(() => import("../Loaders/SpinnerLoader"), {
  ssr: false,
});

interface PropsType {
  handleHideAva: () => void;
}

function UploadImage(props: PropsType) {
  const [image, setImage] = useState<File>();
  const [preview, setPreview] = useState<string>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const dispatch = useDispatch();
  const router = useRouter();

  function handleChooseFile(e: React.FormEvent<HTMLInputElement>) {
    const target = e.target as HTMLInputElement;
    if (target.files && target.files.length) {
      console.log(target.files[0]);
      setPreview(URL.createObjectURL(target.files[0]));
      setImage(target.files[0]);
    }
  }

  function handleUpload() {
    setIsLoading(true);
    const user = userService.getUser();
    if (image && user) {
      const formData = new FormData();
      formData.append("avatar", image);
      axios
        .patch(
          process.env.NEXT_PUBLIC_SERVER_URL + USER_UPDATE_INFO_ENDPOINT,
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: `Bearer ${user.accessToken.token}`,
            },
          }
        )
        .then((res) => {
          setIsLoading(false);
          toast.success("Cập nhật ảnh đại diện thành công");
          setTimeout(() => {
            router.reload();
          }, 3000);
          console.log(res.data);
        })
        .catch((err) => {
          console.log(err);
          toast.error("Cập nhật ảnh đại diện thất bại");
        });
    }
  }

  function removeReview() {
    setPreview("");
    setImage(undefined);
  }

  return (
    <AnimatePresence exitBeforeEnter>
      <div className="fixed top-0 bottom-0 left-0 right-0 z-20 flex">
        <div className="absolute h-full min-w-full bg-slate-400/30"></div>
        <motion.div
          variants={animations.modalTransition}
          initial="initial"
          animate="animate"
          exit="exit"
          className="z-10 w-3/4 m-auto bg-white rounded-md md:w-1/2"
        >
          <div className="relative flex items-center justify-center py-5 border-b border-tertiary-500 md:p-5 px-7">
            <h3 className="font-semibold text-center uppercase">
              Cập nhật ảnh đại diện
            </h3>
            <button
              className="absolute p-2 text-base leading-3 text-white rounded-full top-4 md:right-6 right-3 bg-primary"
              onClick={props.handleHideAva}
            >
              &times;
            </button>
          </div>
          {preview ? (
            <div className="flex items-center justify-center py-20">
              <img
                src={preview}
                alt="avatar"
                className="w-64 h-64 border-2 border-black rounded-full"
              />
            </div>
          ) : (
            <div className="flex px-8 py-16">
              <div className="m-auto text-center ">
                <div>
                  <Image
                    src="/image/icon/bi_image.jpg"
                    alt="image"
                    width={70}
                    height={70}
                  />
                </div>
                <span className="block mb-3 text-tertiary-400">
                  Chọn ảnh hoặc kéo thả ảnh
                </span>

                <div className="relative px-4 py-2 m-auto text-white rounded-lg cursor-pointer bg-primary w-fit ">
                  <label
                    htmlFor="upload-image"
                    className="min-w-full cursor-pointer"
                  >
                    Chọn ảnh
                  </label>
                  <input
                    type="file"
                    id="upload-image"
                    className="absolute z-0 opacity-0 pointer-events-none"
                    accept="image/png, image/jpeg"
                    onChange={(e: React.FormEvent<HTMLInputElement>) =>
                      handleChooseFile(e)
                    }
                  />
                </div>
              </div>
            </div>
          )}
          <div className="flex flex-row items-center justify-center md:py-5 md:space-x-20">
            <div>
              <button
                className="px-4 py-2 text-white shadow-lg md:w-32 bg-primary rounded-xl"
                onClick={removeReview}
              >
                Chọn lại
              </button>
            </div>
            <div>
              {isLoading ? (
                <SpinnerLoader />
              ) : (
                <button
                  className="px-8 py-2 text-white shadow-lg md:w-32 bg-primary rounded-xl"
                  onClick={handleUpload}
                >
                  Lưu
                </button>
              )}
            </div>
          </div>
        </motion.div>
      </div>
    </AnimatePresence>
  );
}

export default UploadImage;
