/** @type {import('next').NextConfig} */
const withBundleAnalyzer = require("@next/bundle-analyzer")({
	enabled: process.env.ANALYZE === "true",
});

// module.exports = withBundleAnalyzer({
// 	reactStrictMode: true,
// 	swcMinify: true,
// 	images: {
// 		domains: ["picsum.photos"],
// 	},
// 	env: {
// 		NEXT_PUBLIC_SERVER_URL: "http://www.lantoathongdiep.com",
// 	},
// });

module.exports = {
	reactStrictMode: true,
	swcMinify: true,
	images: {
		domains: ["picsum.photos", "res.cloudinary.com"],
	},
	env: {
		NEXT_PUBLIC_SERVER_URL: "https://api.wecharity.com.vn",
	},
};
