module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    screens: {
      xs: "575px", // Mobile (iPhone 3 - iPhone XS Max).
      sm: "576px", // Mobile (matches max: iPhone 11 Pro Max landscape @ 896px).
      "tablet-sm": "768px",
      md: "898px", // Tablet (matches max: iPad Pro @ 1112px).
      lg: "1200px", // Desktop smallest.
      xl: "1159px", // Desktop wide.
      xxl: "1359px", // Desktop widescreen.
    },
    extend: {
      colors: {
        primary: "#00c2ff",
        secondary: {
          100: "#F2FCFF",
          200: "#3B94C7",
          300: "#6BBBE9",
          400: "#94CAE9",
          500: "#C0F0FF",
        },
        tertiary: {
          100: "#D3E2D4",
          200: "#FDFDFD",
          300: "#393838",
          400: "#8B8B8B",
          500: "#C4C4C4",
          600: "#5C5C5C",
          700: "#F1F1F1",
        },
        danger: "#FF0000",
        warning: "#FFF050",
      },
    },
    backgroundImage: {
      "line-1": "url('/image/line/line-1.jpg')",
      "line-2": "url('/image/line/line-2.jpg')",
      "line-3": "url('/image/line/line-3.jpg')",
      "line-4": "url('/image/line/line-4.jpg')",
      "line-5": "url('/image/line/line-5.png')",
      project: "url('/image/project/bg-project.jpg')",
      "primary-to-b":
        "linear-gradient(180deg, #CFF0FA 0%, rgba(237, 244, 255, 0) 100%)",
    },
  },
  plugins: [require("@themesberg/flowbite/plugin")],
};
