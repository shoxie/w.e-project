import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import { RiHandHeartLine } from "react-icons/ri";
import { MdEdit } from "react-icons/md";
import { AiOutlineHeart } from "react-icons/ai";
import { IoIosMan, IoIosMail, IoMdHeart } from "react-icons/io";
import {
  BsBuilding,
  BsFillTelephoneFill,
  BsFillPenFill,
  BsFillCameraFill,
} from "react-icons/bs";
import { motion } from "framer-motion";
import animations from "./../../constants/animation";
import dynamic from "next/dynamic";
import { useSelector } from "react-redux";
import { IRootState } from "../../app/redux/store";
import { useDispatch } from "react-redux";
import { useEffect, useCallback } from "react";
import { getUserInfo } from "../../app/redux/userSlice";
import { useState } from "react";

const UploadImageAva = dynamic(
  () => import("../../components/Modal/UploadImageAva"),
  { ssr: true }
);
const UploadImageCover = dynamic(
  () => import("../../components/Modal/UploadImageCover"),
  { ssr: true }
);
const Avatar = dynamic(() => import("../../components/User/Avatar"), {
  ssr: true,
});

const UserPage: NextPage = () => {
  const dispatch = useDispatch();
  const userData = useSelector((state: IRootState) => state.user);

  const [showModalAva, setShowModalAva] = useState(false);
  const [showModalCover, setShowModalCover] = useState(false);
  const [contextMenuPos, setContextMenuPos] = useState<{
    pageX: number;
    pageY: number;
  }>({
    pageX: 0,
    pageY: 0,
  });
  const [showContextMenu, setShowContextMenu] = useState(false);
  const [currentItem, setCurrentItem] = useState(null);

  const handleShowAva = () => {
    setShowModalAva(true);
  };

  const handleHideAva = () => {
    setShowModalAva(false);
  };

  const handleShowCover = () => {
    setShowModalCover(true);
  };

  const handleHideCover = () => {
    setShowModalCover(false);
  };

  useEffect(() => {
    dispatch(getUserInfo());
  }, []);

  function handleContextMenu(e: React.MouseEvent, item: any) {
    setContextMenuPos({
      pageX: e.pageX,
      pageY: e.pageY,
    });
    setCurrentItem(item);
    setShowContextMenu(true);
  }
  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="px-0 mx-auto md:px-16 max-w-screen-xxl"
    >
      <div className="grid items-start w-full grid-cols-1 px-5 py-24 space-y-10 md:grid-cols-2 md:space-x-10 md:space-y-0 md:px-0">
        <div className="flex flex-col items-center">
          {/*profile cover*/}
          <div className="w-full px-5 overflow-hidden rounded-xl h-72 md:shadow-xl md:px-0">
            <img
              src={userData.anhbia ?? "https://picsum.photos/1920/1080"}
              // width={500}
              // height={500}
              className="object-cover w-full h-full rounded-xl"
              alt="profile-cover"
            />
          </div>
          <div className="flex flex-col items-center w-3/4 px-5 -mt-16 md:-mt-36 md:px-0">
            <div className="flex flex-col pt-5 items-flex-start md:justify-start md:items-start md:w-auto md:pt-0">
              {/*user profile picture */}
              <div className="px-6 py-3 m-auto -mt-3 xs:w-max">
                <Avatar
                  className="w-64 h-64"
                  rank={5}
                  image={userData.avatar ?? "https://picsum.photos/512/512"}
                  handleUpload={handleShowAva}
                />
              </div>
              {/*user name and points */}
              <div className="flex flex-col items-center w-full pb-8 space-y-3 text-center">
                <div className="">
                  <span className="text-4xl font-bold text-center">
                    {userData.hoten ?? "Nguyễn Văn A"}
                  </span>
                </div>
                <div className="flex items-center space-x-2 text-primary">
                  <RiHandHeartLine className="text-2xl" />
                  <span>{userData.diemso ?? 0} points</span>
                </div>
                <div className="block md:hidden">
                  <Link href="/user/update">
                    <a className="flex items-center p-2 text-white rounded-lg bg-primary md:space-x-1">
                      <MdEdit className="text-lg" />
                      <span>Chỉnh sửa thông tin cá nhân</span>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            {/*edit button*/}
            <div className="hidden pb-10 mt-auto md:block">
              <Link href="/user/update">
                <a className="flex items-center p-2 text-white rounded-lg bg-primary md:space-x-1">
                  <MdEdit className="text-lg" />
                  <span>Chỉnh sửa thông tin cá nhân</span>
                </a>
              </Link>
            </div>
          </div>
          <div className="flex items-center justify-center w-full mb-10">
            <div className="p-10 border rounded-lg shadow-lg">
              <div className="flex items-baseline justify-between mb-4">
                <h4 className="text-3xl font-semibold">Giới thiệu</h4>
                <BsFillPenFill className="ml-4 text-xl cursor-pointer text-primary" />
              </div>
              <div className="flex flex-col items-start pr-20">
                <div className="flex items-center justify-start mb-4 space-x-6">
                  <IoIosMan className="text-2xl text-primary" />
                  <span className="text-lg">Chức vụ</span>
                </div>
                <div className="flex items-center justify-start mb-4 space-x-6">
                  <BsBuilding className="text-2xl text-primary" />
                  <span className="text-lg">Nơi công tác</span>
                </div>
                <div className="flex items-center justify-start mb-4 space-x-6">
                  <BsFillTelephoneFill className="text-2xl text-primary" />
                  <span className="text-lg">Số điện thoại</span>
                </div>
                <div className="flex items-center justify-start mb-4 space-x-6">
                  <IoIosMail className="text-2xl text-primary" />
                  <span className="text-lg">Email</span>
                </div>
              </div>
            </div>
          </div>
          <div className="flex items-center justify-center w-full">
            <div className="p-10 border rounded-lg shadow-lg ">
              <div className="flex justify-center w-full space-x-5">
                {[1, 2, 3, 4, 5].map((item) => (
                  <div key={item}>
                    <AiOutlineHeart className="text-3xl text-primary" />
                  </div>
                ))}
              </div>
              <div className="flex justify-center w-full py-5">
                <Link href="/user/achieve">
                  <a className="px-4 py-2 text-white shadow-md rounded-xl bg-primary">
                    Achievements
                  </a>
                </Link>
              </div>
              <div className="flex flex-col items-center w-full space-y-5">
                {[1, 2, 3, 4, 5].map((item) => (
                  <div key={item}>Lorem ipsum dolor sit amet</div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div>
          <div className="flex flex-col items-center space-y-10 justify-s">
            {[1, 2, 3, 4, 5].map((item, idx) => (
              <motion.div
                key={item}
                initial={{ opacity: 0, translateY: -50 }}
                animate={{
                  opacity: 1,
                  translateY: 0,
                }}
                transition={{ duration: 0.4, delay: idx * 0.4 }}
                whileInView={{
                  opacity: 1,
                  translateY: 0,
                }}
                onClick={(e) => handleContextMenu(e, item)}
              >
                <div className="relative project-item">
                  <img
                    src="https://picsum.photos/1920/1080"
                    className="shadow-md rounded-xl"
                    alt="Lorem"
                  />
                  <div className="absolute top-0 left-0 flex invisible w-full h-full transition-all opacity-0 cursor-pointer bg-tertiary-300 rounded-xl project-item_change-image">
                    <BsFillCameraFill className="m-auto text-3xl text-white" />
                  </div>
                </div>
                <div className="flex items-center justify-between pt-2 mt-4 font-semibold">
                  <span>Lorem ipsum dolor sit amet</span>
                  <span className="flex items-center">
                    10K <IoMdHeart className="ml-3 text-lg text-primary" />
                  </span>
                </div>
              </motion.div>
            ))}
          </div>
        </div>
      </div>
      {showModalAva && <UploadImageAva handleHideAva={handleHideAva} />}
      {showModalCover && <UploadImageCover handleHideCover={handleHideCover} />}
      {showContextMenu && (
        <div
          className="fixed z-50 w-64 bg-white rounded-lg"
          style={{
            top: contextMenuPos.pageY,
            left: contextMenuPos.pageX,
          }}
          onMouseLeave={() => setShowContextMenu(false)}
        >
          <div className="flex flex-col items-start">
            <button className="w-full p-4 text-left rounded-t-lg hover:bg-gray-100 hover:text-primary">
              Đổi ảnh
            </button>
            <button className="w-full p-4 text-left hover:bg-gray-100 hover:text-primary">
              Đổi tên dự án
            </button>
            <button className="w-full p-4 text-left hover:bg-gray-100 hover:text-primary">
              Thông tin chi tiết
            </button>
            <button className="w-full p-4 text-left rounded-b-lg hover:bg-gray-100 hover:text-primary">
              Hủy
            </button>
          </div>
        </div>
      )}
    </motion.div>
  );
};

export default UserPage;
