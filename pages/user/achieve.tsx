import type { NextPage } from "next";
import { Disclosure } from "@headlessui/react";
import { FiChevronUp } from "react-icons/fi";
import animations from "./../../constants/animation";
import { motion } from "framer-motion";
import AchievementLayout from "../../components/Layout/User/AchievementLayout";

function AchieveInfo() {
  return (
    <div className="w-full px-4">
      <div className="w-full mx-auto bg-white rounded-2xl">
        <Disclosure as="div" className="w-full p-5 border shadow-md rounded-xl">
          {({ open }) => (
            <>
              <Disclosure.Button className="flex items-center justify-between w-full">
                <span className="text-xl font-semibold uppercase">
                  Lorem ipsum dolor sit amet
                </span>
                <FiChevronUp
                  className={`${
                    open ? "transform rotate-180" : ""
                  } w-5 h-5 text-primary`}
                />
              </Disclosure.Button>
              <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                <div className="text-lg text-justify bg-white">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem
                  ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum
                  dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor
                  sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit
                  amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet,
                  consectetur adipiscing elit. Lorem ipsum dolor sit amet,
                  consectetur adipiscing elit. Lorem ipsum dolor sit amet,
                  consectetur adipiscing elit. Lorem ipsum dolor sit amet,
                </div>
              </Disclosure.Panel>
            </>
          )}
        </Disclosure>
      </div>
    </div>
  );
}

const UserPage = () => {
  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="px-0 mx-auto bg-line-2 max-w-screen-xxl"
    >
      <div className="flex flex-col w-full space-y-5 md:items-center">
        {[1, 2, 3, 4, 5].map((item) => (
          <AchieveInfo key={item} />
        ))}
      </div>
      <div className="mt-6 px-4 flex">
        <button className="ml-auto px-4 py-2 bg-primary text-white rounded-xl shadow-lg ">
          See more
        </button>
      </div>
    </motion.div>
  );
};

UserPage.Layout = AchievementLayout;

export default UserPage;
