import { NextFetchEvent, NextRequest, NextResponse } from "next/server";
import userService from "../../app/service/user.service";
import Cookies from "js-cookie";
import Router from "next/router";

export function middleware(req: NextRequest, event: NextFetchEvent) {
  if (!req.cookies.user) {
    return NextResponse.redirect("/login");
  } else {
    return NextResponse.next();
  }
}
