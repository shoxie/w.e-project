import type { NextPage } from "next";
import Image from "next/image";
import { useState, useEffect } from "react";
import { RiHandHeartLine } from "react-icons/ri";
import { AiFillCheckCircle } from "react-icons/ai";
import { Disclosure, Transition } from "@headlessui/react";
import { FiChevronUp } from "react-icons/fi";
import { FaCamera } from "react-icons/fa";
import animations from "./../../constants/animation";
import { motion } from "framer-motion";
import dynamic from "next/dynamic";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../app/redux/store";
import { getUserInfo } from "../../app/redux/userSlice";
import { UserBasicInfo } from "../../constants/form.spec";
import { updateUserInfo } from "../../app/redux/userSlice";

const UploadImageAva = dynamic(
  () => import("../../components/Modal/UploadImageAva"),
  { ssr: true }
);
const UploadImageCover = dynamic(
  () => import("../../components/Modal/UploadImageCover"),
  { ssr: true }
);
const Avatar = dynamic(() => import("../../components/User/Avatar"), {
  ssr: false,
});

function ProgressBar({ level }: { level: number }) {
  switch (level) {
    case 1:
      return <div className="w-1/3 h-2 rounded-full bg-danger"></div>;
    case 2:
      return <div className="w-2/3 h-2 rounded-full bg-warning"></div>;
    case 3:
      return <div className="w-full h-2 rounded-full bg-primary"></div>;
    default:
      return <div className="w-1/3 h-2 rounded-full bg-danger"></div>;
  }
}

function UpdateUserInfo() {
  const dispatch = useDispatch();
  var userData = useSelector(
    (state: IRootState) => state.user
  ) as UserBasicInfo;
  const [values, setValues] = useState<UserBasicInfo>({
    address: "",
    anhbia: "",
    chucvu: "",
    email: "",
    dateofbirth: "",
    hoten: "",
    noicongtac: "",
    sdt: "",
    noisinh: "",
  });
  useEffect(() => {
    dispatch(getUserInfo());
  }, []);

  useEffect(() => {
    setValues({
      address: userData.address,
      anhbia: userData.anhbia,
      chucvu: userData.chucvu,
      email: userData.email,
      dateofbirth: userData.dateofbirth,
      hoten: userData.hoten,
      noicongtac: userData.noicongtac,
      sdt: userData.sdt,
      noisinh: userData.noisinh,
    });
  }, [userData]);
  const handleChange =
    (prop: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
      event.preventDefault();
      setValues({ ...values, [prop]: event.target.value });
    };

  function updateUser(event: React.MouseEvent) {
    event.preventDefault();

    dispatch(
      updateUserInfo({
        address: values.address,
        anhbia: values.anhbia,
        chucvu: values.chucvu,
        email: values.email,
        dateofbirth: values.dateofbirth,
        hoten: values.hoten,
        noicongtac: values.noicongtac,
        sdt: values.sdt,
        noisinh: values.noisinh,
      })
    );
  }
  return (
    <div className="w-full px-4">
      <div className="w-full mx-auto bg-white rounded-2xl">
        <Disclosure as="div" className="w-full p-5 border shadow-md rounded-xl">
          {({ open }) => (
            <>
              <Disclosure.Button className="flex items-center justify-between w-full">
                <span className="text-xl font-semibold uppercase">
                  Thông tin cá nhân
                </span>
                <FiChevronUp
                  className={`${
                    open ? "transform rotate-180" : ""
                  } w-5 h-5 text-primary`}
                />
              </Disclosure.Button>
              <Transition
                enter="transition duration-100 ease-out"
                enterFrom="transform scale-95 opacity-0"
                enterTo="transform scale-100 opacity-100"
                leave="transition duration-75 ease-out"
                leaveFrom="transform scale-100 opacity-100"
                leaveTo="transform scale-95 opacity-0"
              >
                <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                  <form className="mt-6">
                    <div className="relative flex flex-col">
                      <input
                        value={values.hoten}
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("hoten")}
                      />
                      <label
                        htmlFor="fullname"
                        className="label-login form-label"
                      >
                        Họ tên
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        value={values.address}
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("address")}
                      />
                      <label
                        htmlFor="address"
                        className="label-login form-label"
                      >
                        Địa chỉ
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        value={values.sdt}
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("sdt")}
                      />
                      <label htmlFor="phone" className="label-login form-label">
                        Số điện thoại
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        value={values.email}
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("email")}
                      />
                      <label htmlFor="email" className="label-login form-label">
                        Email
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        value={values.dateofbirth}
                        required
                        type="date"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("dateofbirth")}
                      />
                      <label
                        htmlFor="password"
                        className="label-login form-label"
                      >
                        Ngày sinh
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        value={values.noisinh}
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("noisinh")}
                      />
                      <label
                        htmlFor="password"
                        className="label-login form-label"
                      >
                        Nơi sinh
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        value={values.chucvu}
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("chucvu")}
                      />
                      <label
                        htmlFor="password"
                        className="label-login form-label"
                      >
                        Chức vụ
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        value={values.noicongtac}
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                        onChange={handleChange("noicongtac")}
                      />
                      <label
                        htmlFor="password"
                        className="label-login form-label"
                      >
                        Nơi công tác
                      </label>
                    </div>
                    <div className="w-full p-2 mt-8 text-lg text-center text-white bg-primary rounded-xl ">
                      <button
                        className="w-full cursor-pointer"
                        onClick={updateUser}
                      >
                        Cập nhật
                      </button>
                    </div>
                  </form>
                </Disclosure.Panel>
              </Transition>
            </>
          )}
        </Disclosure>
      </div>
    </div>
  );
}

function UpdateBankInfo() {
  return (
    <div className="w-full px-4 pt-5 md:pt-10">
      <div className="w-full mx-auto bg-white rounded-2xl">
        <Disclosure as="div" className="w-full p-5 border shadow-md rounded-xl">
          {({ open }) => (
            <>
              <Disclosure.Button className="flex items-center justify-between w-full">
                <span className="text-xl font-semibold uppercase">
                  Tài khoản ngân hàng
                </span>
                <FiChevronUp
                  className={`${
                    open ? "transform rotate-180" : ""
                  } w-5 h-5 text-primary`}
                />
              </Disclosure.Button>
              <Transition
                enter="transition duration-100 ease-out"
                enterFrom="transform scale-95 opacity-0"
                enterTo="transform scale-100 opacity-100"
                leave="transition duration-75 ease-out"
                leaveFrom="transform scale-100 opacity-100"
                leaveTo="transform scale-95 opacity-0"
              >
                <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
                  <form className="mt-6">
                    <div className="relative flex flex-col">
                      <input
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                      />
                      <label
                        htmlFor="fullname"
                        className="label-login form-label"
                      >
                        Số tài khoản
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        required
                        type="password"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                      />
                      <label
                        htmlFor="address"
                        className="label-login form-label"
                      >
                        Tên chủ thẻ
                      </label>
                    </div>
                    <div className="relative flex flex-col mt-7">
                      <input
                        required
                        type="text"
                        className="border input-login form-input border-tertiary-500"
                        placeholder=" "
                      />
                      <label htmlFor="phone" className="label-login form-label">
                        Ngân hàng
                      </label>
                    </div>

                    <div className="w-full p-2 mt-8 text-lg text-center text-white bg-primary rounded-xl ">
                      <button type="submit" className="w-full cursor-pointer">
                        Cập nhật
                      </button>
                    </div>
                  </form>
                </Disclosure.Panel>
              </Transition>
            </>
          )}
        </Disclosure>
      </div>
    </div>
  );
}

const UserPage: NextPage = () => {
  const [showModalAva, setShowModalAva] = useState(false);
  const [showModalCover, setShowModalCover] = useState(false);

  const handleShowAva = () => {
    setShowModalAva(true);
  };

  const handleHideAva = () => {
    setShowModalAva(false);
  };

  const handleShowCover = () => {
    setShowModalCover(true);
  };

  const handleHideCover = () => {
    setShowModalCover(false);
  };

  const dispatch = useDispatch();
  const userData = useSelector((state: IRootState) => state.user);

  useEffect(() => {
    dispatch(getUserInfo());
  }, []);

  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="px-0 mx-auto md:px-16 bg-line-2 max-w-screen-xxl"
    >
      <div className="grid items-start w-full grid-cols-1 px-5 py-24 space-y-10 md:grid-cols-2 md:space-x-10 md:space-y-0 md:px-0">
        <div className="flex flex-col items-center">
          {/*profile cover*/}
          <div className="relative w-full px-5 overflow-hidden rounded-xl h-72 md:shadow-xl md:px-0">
            <img
              src={userData.anhbia ?? "https://picsum.photos/1920/1080"}
              // width={1920}
              // height={600}
              className="object-cover w-full h-full cursor-pointer rounded-xl"
              alt="profile-cover"
            />
            <button
              className="absolute p-2 bg-white rounded-full cursor-pointer bottom-4 lg:right-4 right-8"
              onClick={handleShowCover}
            >
              <FaCamera className="text-xl lg:text-3xl text-primary" />
            </button>
          </div>
          <div className="flex flex-col items-center w-full px-5 -mt-16 md:-mt-36 md:px-0">
            <div className="flex flex-col items-center pt-5 flex-start md:justify-start md:items-center md:w-auto md:space-x-5 md:pt-0">
              {/*user profile picture */}
              <div className="z-20 px-6 py-3 m-auto -mt-3 xs:w-max">
                <Avatar
                  className="w-64 h-64"
                  rank={5}
                  image={userData.avatar ?? "https://picsum.photos/512/512"}
                  handleUpload={handleShowAva}
                />
              </div>
              {/*user name and points */}
              <div className="flex flex-col items-center pb-8 mt-4 space-y-3">
                <div className="">
                  <span className="text-4xl font-bold text-center">
                    {userData.hoten ?? "Nguyễn Văn A"}
                  </span>
                </div>
                <div className="flex items-center space-x-2 font-semibold text-primary">
                  <RiHandHeartLine className="text-2xl" />
                  <span>{userData.diemso ?? 0} points</span>
                </div>
              </div>
            </div>
          </div>
          <div className="flex items-center justify-center w-full ">
            <div className="px-10 py-6 bg-white border rounded-lg shadow-lg">
              <div className="pb-3 text-center">
                <span className="text-xl font-semibold uppercase">
                  hoàn thiện tài khoản
                </span>
              </div>
              <ProgressBar level={2} />
              <div className="flex flex-row items-center pt-5 space-x-5 text-lg">
                <AiFillCheckCircle className="text-primary" />
                <span>Xác thực tài khoản qua email</span>
              </div>
              <div className="flex flex-row items-center pt-5 space-x-5 text-lg">
                <AiFillCheckCircle className="text-primary" />
                <span>Xác minh số điện thoại</span>
              </div>
              <div className="flex flex-row items-center pt-5 space-x-5 text-lg">
                <AiFillCheckCircle className="text-primary" />
                <span>Cập nhật thông tin cá nhân</span>
              </div>
            </div>
          </div>
        </div>
        <div className="">
          <UpdateUserInfo />
          <UpdateBankInfo />
        </div>
      </div>
      {showModalAva && <UploadImageAva handleHideAva={handleHideAva} />}
      {showModalCover && <UploadImageCover handleHideCover={handleHideCover} />}
    </motion.div>
  );
};

export default UserPage;
