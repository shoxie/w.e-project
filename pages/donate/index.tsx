import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import { motion } from "framer-motion";
import animations from "../../constants/animation";
import dynamic from "next/dynamic";

const Home: NextPage = () => {
  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="mt-24"
    >
      <div className="pb-24 md:pb-0 bg-line-2 bg-contain bg-no-repeat">
        <div className="py-20 ">
          <div className="flex flex-col items-center justify-center gap-8 mx-auto max-w-screen-xxl">
            <h2 className="text-4xl font-semibold text-center">
              Các hình thức thanh toán
            </h2>
            <p className="w-4/6 text-center">
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout. The
              point of using Lorem Ipsum is that it has a more-or-less normal
              distribution of letters, as opposed to using Content here, content
              here, making it look like readable English. Many desktop
              publishing packages and web page editors now use Lorem Ipsum as
              their default model text, and a search for lorem ipsum will
              uncover many web sites still in their infancy. Various versions
              have evolved over the years, sometimes by accident, sometimes on
              purpose (injected humour and the like).
            </p>
          </div>
        </div>
        <div className="w-full px-16 py-10 mt-10 bg-no-repeat bg-cover bg-project min-h-screen">
          <h2 className="text-4xl font-semibold text-center">Coming soon</h2>
        </div>
      </div>
    </motion.div>
  );
};

export default Home;
