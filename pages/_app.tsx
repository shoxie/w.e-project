import "../styles/globals.css";
import type { AppProps } from "next/app";
import HomeLayout from "../components/Layout/Home";
import { SWRConfig } from "swr";
import axiosClient from "../app/axiosClient";
import { store } from "../app/redux/store";
import { Provider } from "react-redux";
import { AnimatePresence } from "framer-motion";
import "react-toastify/dist/ReactToastify.css";

const fetcher = async (url: string) => {
  try {
    const res = await axiosClient.get(url);
    return res;
  } catch (err) {
    throw err;
  }
};

function MyApp({
  Component,
  pageProps,
}: AppProps & { Component: { Layout?: any } }) {
  const Layout = Component.Layout || HomeLayout;
  return (
    <SWRConfig
      value={{
        fetcher: fetcher,
        revalidateOnFocus: false,
      }}
    >
      <Provider store={store}>
        <AnimatePresence exitBeforeEnter>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </AnimatePresence>
      </Provider>
    </SWRConfig>
  );
}

export default MyApp;
