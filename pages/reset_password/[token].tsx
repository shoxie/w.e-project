import { useEffect } from "react";
import LoginLayout from "../../components/Layout/Login";
import { GetServerSideProps } from "next";
import Image from "next/image";
import * as React from "react";
import { motion } from "framer-motion";
import animations from "../../constants/animation";
import Link from "next/link";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch } from "react-redux";
import { resetPassword } from "../../app/redux/authSlice";
import { ResetPasswordPayload } from "../../constants/request.spec";
import { useRouter } from "next/router";

interface PropsType {
  token: string;
}

interface FormPassword {
  password: string;
  repassword: string;
}

const NewPasswordView = (props: PropsType) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormPassword>();
  const onSubmit: SubmitHandler<FormPassword> = (data) => {
    //send api
    dispatch(resetPassword({ newPassword: data.password, token: props.token }));
  };

  useEffect(() => {
    if (!props.token) {
      router.push("/login");
    }
  });

  return (
    <div className="relative flex items-center w-full min-h-screen">
      <section className="flex items-center justify-center w-full gap-4 px-16 py-4 mt-20 xxl:mt-0">
        <div className="flex justify-center w-1/2">
          <div className="p-8 bg-white border rounded-md lg:w-9/12 sm:w-full border-tertiary-500">
            <h3 className="text-2xl font-semibold text-center">
              TẠO MẬT KHẨU MỚI
            </h3>
            <div className="flex justify-center w-full mt-3 text-center">
              <span className="block w-3/4 text-xs text-tertiary-600">
                Đặt lại mật khẩu mới để truy cập vào tài khoản
              </span>
            </div>
            <form className="mt-6" onSubmit={handleSubmit(onSubmit)}>
              <div className="relative flex flex-col">
                <input
                  type="password"
                  className={
                    errors.password
                      ? "input-login form-input border border-red-500"
                      : "input-login form-input border border-tertiary-500"
                  }
                  placeholder=" "
                  {...register("password", { required: true })}
                />
                <label
                  htmlFor="password"
                  className={
                    errors.password
                      ? "label-login form-label text-red-500"
                      : "label-login form-label text-tertiary-500"
                  }
                >
                  Mật Khẩu
                </label>
                {errors.password && (
                  <p className="pl-2 mt-1 text-xs italic text-red-500">
                    Mật khẩu không được để trống
                  </p>
                )}
              </div>
              <div className="relative flex flex-col mt-7">
                <input
                  type="password"
                  className={
                    errors.repassword
                      ? "input-login form-input border border-red-500"
                      : "input-login form-input border border-tertiary-500"
                  }
                  placeholder=" "
                  {...register("repassword", { required: true })}
                />
                <label
                  htmlFor="password-confirm"
                  className={
                    errors.repassword
                      ? "label-login form-label text-red-500"
                      : "label-login form-label text-tertiary-500"
                  }
                >
                  Nhập Lại Mật Khẩu
                </label>
                {errors.repassword && (
                  <p className="pl-2 mt-1 text-xs italic text-red-500">
                    Mật khẩu không được để trống
                  </p>
                )}
              </div>
              <div className="w-full p-2 mt-8 text-center text-white bg-primary rounded-xl">
                <input
                  type="submit"
                  className="w-full cursor-pointer"
                  value="Xác nhận"
                />
              </div>
            </form>
          </div>
        </div>
      </section>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const token = context.query.token;
  return {
    props: { token },
  };
};

NewPasswordView.Layout = LoginLayout;

export default NewPasswordView;
