import { motion } from "framer-motion";
import Image from "next/image";
import { useState, useEffect } from "react";
import RankingLayout from "../../components/Layout/Ranking";
import animations from "../../constants/animation";
import { useDispatch, useSelector } from "react-redux";
import { getAllRanking } from "../../app/redux/rankSlice";
import { IRootState } from "../../app/redux/store";

const RankingPage = () => {
  const [activeButton, setActiveButton] = useState("left");
  const dispatch = useDispatch();
  const rankingList = useSelector<IRootState>(
    (state) => state.ranking.list
  ) as any;

  useEffect(() => {
    dispatch(getAllRanking());
  }, [dispatch]);

  useEffect(() => {
    console.log("rankingList", rankingList);
  }, [rankingList]);

  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className=""
    >
      <div className="md:px-0 md:mt-0 ">
        <div className="min-h-screen bg-no-repeat bg-cover bg-project">
          <div className="px-5 py-20 bg-cover bg-line-5 md:px-16">
            <div className="flex flex-col items-center justify-center gap-8 mx-auto max-w-screen-xxl">
              <h2 className="text-4xl font-semibold text-center">
                Bảng Xếp Hạng
              </h2>
              <div className="flex items-center overflow-hidden rounded-full bg-secondary-500">
                <button
                  onClick={() => setActiveButton("left")}
                  className={`px-5 py-2 transition-all duration-500 ${
                    activeButton === "left"
                      ? "bg-primary text-white rounded-r-full"
                      : "bg-secondary-500 text-black"
                  }`}
                >
                  Hôm nay
                </button>
                <button
                  onClick={() => setActiveButton("right")}
                  className={`px-5 py-2 transition-all duration-500 ${
                    activeButton === "right"
                      ? "bg-primary text-white rounded-l-full"
                      : "bg-secondary-500 text-black"
                  }`}
                >
                  Tháng này
                </button>
              </div>
              <div className="mt-5">
                <div className="flex w-full">
                  <motion.div
                    initial={{ opacity: 0, y: -100 }}
                    animate={{
                      opacity: 1,
                      y: 0,
                      transition: { delay: 0.8, type: "spring" },
                    }}
                    className="mt-32 mr-10 md:mt-44 md:mr-14"
                  >
                    <div className="relative">
                      <Image
                        src={rankingList[1]?.avatar ?? "/image/user/user.png"}
                        alt="user-img"
                        width="180"
                        height="180"
                        className="shadow-lg"
                      />
                      <div className="absolute -top-2 md:top-2 -right-2 md:right-2 w-[35px] h-[35px] md:w-[45px] md:h-[45px] flex items-center justify-center text-white text-xl border-2 border-slate-50 rounded-full bg-tertiary-500">
                        2
                      </div>
                    </div>
                    <div className="w-full text-2xl font-medium text-center">
                      {rankingList[1]?.hoten}
                    </div>
                  </motion.div>
                  <motion.div
                    initial={{ opacity: 0, y: -100 }}
                    animate={{
                      opacity: 1,
                      y: 0,
                      transition: { delay: 0.5, type: "spring" },
                    }}
                  >
                    <div className="relative">
                      <Image
                        src={rankingList[0]?.avatar ?? "/image/user/user.png"}
                        alt="user-img"
                        width="230"
                        height="230"
                        className="shadow-lg"
                      />
                      <div className="absolute -top-2 md:top-2 -right-2 md:right-2 w-[40px] h-[40px] md:w-[64px] md:h-[64px] flex items-center justify-center text-white text-xl border-2 border-slate-50 rounded-full bg-[#FFB800]">
                        1
                      </div>
                    </div>
                    <div className="w-full text-2xl font-medium text-center">
                      {rankingList[0]?.hoten}
                    </div>
                  </motion.div>
                  <motion.div
                    initial={{ opacity: 0, y: -100 }}
                    animate={{
                      opacity: 1,
                      y: 0,
                      transition: { delay: 1.1, type: "spring" },
                    }}
                    className="mt-32 ml-10 md:mt-44 md:ml-14"
                  >
                    <div className="relative">
                      <Image
                        src={rankingList[2]?.avatar ?? "/image/user/user.png"}
                        alt="user-img"
                        width="180"
                        height="180"
                        className="shadow-lg"
                      />
                      <div className="absolute -top-2 md:top-2 -right-2 md:right-2 w-[35px] h-[35px] md:w-[45px] md:h-[45px] flex items-center justify-center text-white text-xl border-2 border-slate-50 rounded-full bg-[#D77026]">
                        3
                      </div>
                    </div>
                    <div className="w-full text-2xl font-medium text-center">
                      {rankingList[2]?.hoten}
                    </div>
                  </motion.div>
                </div>
              </div>
              <div className="mt-5">
                <div className="flex flex-wrap items-center justify-between gap-x-10 gap-y-14 md:gap-y-16">
                  {rankingList.slice(3).map((item: any) => {
                    return (
                      <div
                        key={item.id}
                        className="px-5 py-3 flex items-start justify-between bg-white rounded-lg shadow-lg min-w-full flex-1 md:min-w-[400px] max-w-[400px]"
                      >
                        <div className="mr-5">
                          <Image
                            src={item?.avatar ?? "/image/user/user.png"}
                            alt="user-img"
                            width="75"
                            height="75"
                          />
                        </div>
                        <div className="px-2 flex flex-col items-start justify-center gap-2 flex-[2_2_0%]">
                          <span className="text-lg font-semibold break-words md:text-xl">
                            {item.hoten}
                          </span>
                          <span>{item.diemso} points</span>
                        </div>
                        <div className="flex items-center justify-center flex-1 gap-2">
                          <span className="text-3xl font-medium">
                            {item.rank}
                          </span>
                          <span className={`text-5xl ${item.color_arrow}`}>
                            {item.arrow}
                          </span>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

RankingPage.Layout = RankingLayout;

export default RankingPage;
