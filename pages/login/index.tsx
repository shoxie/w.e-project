import Image from "next/image";
import * as React from "react";
import { motion } from "framer-motion";
import animations from "./../../constants/animation";
import Link from "next/link";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { signIn } from "../../app/redux/authSlice";
import { LoginPayload } from "../../constants/request.spec";
import dynamic from "next/dynamic";
import { IRootState } from "../../app/redux/store";
const LoginLayout = dynamic(() => import("../../components/Layout/Login"));
const SpinnerLoader = dynamic(
  () => import("../../components/Loaders/SpinnerLoader")
);

const LoginView = () => {
  const isLoading = useSelector<IRootState>((state) => state.loading.isLoading);
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginPayload>();
  const onSubmit: SubmitHandler<LoginPayload> = (data) => {
    //send api
    return dispatch(signIn({ sdt: data.sdt, password: data.password }));
  };

  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="relative flex items-center justify-center w-full min-h-screen bg-login-register-mobile"
    >
      <section className="flex items-center justify-center w-full gap-4 px-16 py-4 mt-20 xxl:mt-0 ">
        <div className="flex justify-center w-9/12 md:w-1/2">
          <div className="px-5 py-8 md:p-8 bg-white border rounded-md lg:w-9/12 xs:w-full border-tertiary-500">
            <h3 className="text-2xl font-semibold text-center">ĐĂNG NHẬP</h3>
            <form className="mt-6" onSubmit={handleSubmit(onSubmit)}>
              <motion.div variants={animations.stagger}>
                <motion.div
                  className="relative flex flex-col"
                  variants={animations.fadeInLeft}
                  initial="initial"
                  animate="animate"
                  exit="exit"
                >
                  <input
                    className={
                      errors.sdt
                        ? "input-login form-input border border-red-500"
                        : "input-login form-input border border-tertiary-500"
                    }
                    placeholder=" "
                    {...register("sdt", { required: true })}
                  />
                  <label
                    htmlFor="phone-number"
                    className={
                      errors.sdt
                        ? "label-login form-label text-red-500"
                        : "label-login form-label text-tertiary-500"
                    }
                  >
                    Số Điện Thoại
                  </label>
                  {errors.sdt && (
                    <p className="pl-2 mt-1 text-xs italic text-red-500">
                      Số điện thoại không được để trống
                    </p>
                  )}
                </motion.div>
                <motion.div
                  className="relative flex flex-col mt-7"
                  variants={animations.fadeInLeft}
                  initial="initial"
                  animate="animate"
                  exit="exit"
                >
                  <input
                    type="password"
                    className={
                      errors.password
                        ? "input-login form-input border border-red-500"
                        : "input-login form-input border border-tertiary-500"
                    }
                    placeholder=" "
                    {...register("password", { required: true })}
                  />
                  <label
                    htmlFor="password"
                    className={
                      errors.password
                        ? "label-login form-label text-red-500"
                        : "label-login form-label text-tertiary-500"
                    }
                  >
                    Mật Khẩu
                  </label>
                  {errors.password && (
                    <p className="pl-2 mt-1 text-xs italic text-red-500">
                      Mật khẩu không được để trống
                    </p>
                  )}
                </motion.div>
                <motion.div
                  className="flex justify-between mt-4 text-xs"
                  variants={animations.fadeInLeft}
                  initial="initial"
                  animate="animate"
                  exit="exit"
                >
                  <div className="flex items-center">
                    <input
                      type="checkbox"
                      id="checkbox"
                      className="mr-2 outline-none border-tertiary-500"
                    />
                    <label
                      htmlFor="checkbox"
                      className="font-semibold text-primary"
                    >
                      Lưu mật khẩu
                    </label>
                  </div>
                  <span>
                    <Link href="/forget-password">
                      <a className="font-semibold text-primary">
                        Bạn quên mật khẩu ?
                      </a>
                    </Link>
                  </span>
                </motion.div>
                <div className="w-full p-2 mt-8 text-center text-white bg-primary rounded-xl">
                  {isLoading ? (
                    <SpinnerLoader />
                  ) : (
                    <button
                      type="submit"
                      className="w-full cursor-pointer text-tertiary-200"
                      value="Đăng nhập"
                    >
                      Đăng nhập
                    </button>
                  )}
                </div>
              </motion.div>
            </form>
            <div className="mt-5 text-sm text-center">
              <span>
                Chưa có tài khoản?{" "}
                <Link href="/register">
                  <a className="font-semibold text-primary">Đăng ký ngay</a>
                </Link>
              </span>
            </div>
          </div>
        </div>
        <div className="justify-center hidden w-1/2 md:flex">
          <Image
            src="/image/login/login.jpg"
            alt="login"
            width={600}
            height={600}
            objectFit="cover"
          />
        </div>
      </section>
    </motion.div>
  );
};

const Login = () => {
  return (
    <>
      <LoginView />
    </>
  );
};

Login.Layout = LoginLayout;

export default Login;
