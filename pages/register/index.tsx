import Image from "next/image";
import { useState } from "react";
import * as React from "react";
import { motion } from "framer-motion";
import animations from "./../../constants/animation";
import Link from "next/link";
import LoginLayout from "../../components/Layout/Login";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { signUp } from "../../app/redux/authSlice";
import { RegisterPayload } from "../../constants/request.spec";
import { IRootState } from "../../app/redux/store";
import SpinnerLoader from "../../components/Loaders/SpinnerLoader";

interface PropsType {
  handleChangeView: (view: string) => void;
  setUser: React.Dispatch<React.SetStateAction<any>>;
  user: RegisterPayload;
}

type FormPassword = {
  password: string;
  repassword: string;
};

const RegisterView = (props: PropsType) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterPayload>();
  const onSubmit: SubmitHandler<RegisterPayload> = (data) => {
    props.handleChangeView("register2");
    props.setUser(data);
  };

  return (
    <>
      <motion.div
        variants={animations.pageTransition}
        initial="initial"
        animate="animate"
        exit="exit"
        className="relative flex items-center justify-center w-full min-h-screen bg-login-register-mobile"
      >
        <section className="flex items-center justify-center w-full gap-4 px-16 py-4 mt-20 xxl:mt-0">
          <div className="flex justify-center w-9/12 md:w-1/2">
            <div className="px-5 py-8 bg-white border rounded-md lg:w-9/12 xs:w-full border-secondary-500">
              <h3 className="text-2xl font-semibold text-center">ĐĂNG KÝ</h3>
              <form className="mt-6 text-md" onSubmit={handleSubmit(onSubmit)}>
                <div className="relative flex flex-col">
                  <input
                    className={
                      errors.hoten
                        ? "input-login form-input border border-red-500"
                        : "input-login form-input border border-tertiary-500"
                    }
                    placeholder=" "
                    {...register("hoten", { required: true })}
                  />
                  <label
                    htmlFor="name"
                    className={
                      errors.hoten
                        ? "label-login form-label text-red-500"
                        : "label-login form-label text-tertiary-500"
                    }
                  >
                    Họ Tên
                  </label>
                  {errors.hoten && (
                    <p className="pl-2 mt-1 text-xs italic text-red-500">
                      Họ và tên không được để trống
                    </p>
                  )}
                </div>
                <div className="relative flex flex-col mt-7">
                  <input
                    className={
                      errors.sdt
                        ? "input-login form-input border border-red-500"
                        : "input-login form-input border border-tertiary-500"
                    }
                    placeholder=" "
                    {...register("sdt", { required: true })}
                  />
                  <label
                    htmlFor="phone-number"
                    className={
                      errors.sdt
                        ? "label-login form-label text-red-500"
                        : "label-login form-label text-tertiary-500 pl-2"
                    }
                  >
                    Số Điện Thoại
                  </label>
                  {errors.sdt && (
                    <p className="pl-2 mt-1 text-xs italic text-red-500">
                      Số điện thoại không được để trống
                    </p>
                  )}
                </div>
                <div className="relative flex flex-col mt-7">
                  <input
                    type="email"
                    className={
                      errors.email
                        ? "input-login form-input border border-red-500"
                        : "input-login form-input border border-tertiary-500"
                    }
                    placeholder=" "
                    {...register("email", { required: true })}
                  />
                  <label
                    htmlFor="phone-number"
                    className={
                      errors.email
                        ? "label-login form-label text-red-500"
                        : "label-login form-label text-tertiary-500"
                    }
                  >
                    Email
                  </label>
                  {errors.email && (
                    <p className="pl-2 mt-1 text-xs italic text-red-500">
                      Email không được để trống
                    </p>
                  )}
                </div>
                <div className="relative flex flex-col mt-7">
                  <input
                    type="text"
                    className={
                      errors.address
                        ? "input-login form-input border border-red-500"
                        : "input-login form-input border border-tertiary-500"
                    }
                    placeholder=" "
                    {...register("address", { required: true })}
                  />
                  <label
                    htmlFor="phone-number"
                    className={
                      errors.address
                        ? "label-login form-label text-red-500"
                        : "label-login form-label text-tertiary-500"
                    }
                  >
                    Địa Chỉ
                  </label>
                  {errors.address && (
                    <p className="pl-2 mt-1 text-xs italic text-red-500">
                      Địa chỉ không được để trống
                    </p>
                  )}
                </div>
                <div className="w-full p-2 mt-8 text-center text-white bg-primary rounded-xl ">
                  <button type="submit" className="w-full cursor-pointer">
                    Tiếp tục
                  </button>
                </div>
              </form>
              <div className="mt-5 text-sm text-center">
                <span>
                  Đã có tài khoản?{" "}
                  <Link href="/login">
                    <a className="text-primary font-semibold">Đăng nhập</a>
                  </Link>
                </span>
              </div>
            </div>
          </div>
          <div className="justify-center hidden w-1/2 md:flex">
            <Image
              src="/image/login/signup-2.jpg"
              alt="signup"
              width={600}
              height={600}
              objectFit="cover"
            />
          </div>
        </section>
      </motion.div>
    </>
  );
};

const RegisterView2 = (props: PropsType) => {
  const [accept, setAccept] = useState(false);
  const [ringAnimation, setRingAnimation] = useState<string>("");
  const dispatch = useDispatch();
  const isLoading = useSelector<IRootState>((state) => state.loading.isLoading);
  const {
    register: register2,
    handleSubmit: handleSubmit2,
    formState: { errors: errors2 },
  } = useForm<FormPassword>();
  const onSubmit: SubmitHandler<FormPassword> = async (data) => {
    if (accept) {
      dispatch(
        signUp({
          ...props.user,
          password: data.password,
        })
      );
    } else {
      setRingAnimation("animate-shake");
      setTimeout(() => {
        setRingAnimation("");
      }, 1000);
    }
  };

  const handleChangeAccept = () => {
    setAccept(!accept);
  };

  const returnLastPage = () => {
    props.handleChangeView("register");
  };

  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="relative flex items-center justify-center w-full min-h-screen"
    >
      <section className="flex items-center justify-center w-full gap-4 px-16 py-4 mt-20 xxl:mt-0">
        <div className="flex justify-center w-1/2">
          <div className="p-8 bg-white border rounded-md lg:w-9/12 sm:w-full border-secondary-500">
            <h3 className="text-2xl font-semibold text-center">ĐĂNG KÝ</h3>
            <form className="mt-6 text-md" onSubmit={handleSubmit2(onSubmit)}>
              <div className="relative flex flex-col">
                <input
                  type="password"
                  className={
                    errors2.password
                      ? "input-login form-input border border-red-500"
                      : "input-login form-input border border-tertiary-500"
                  }
                  placeholder=" "
                  {...register2("password", { required: true, minLength: 8 })}
                />
                <label
                  htmlFor="password"
                  className={
                    errors2.password
                      ? "label-login form-label text-red-500"
                      : "label-login form-label text-tertiary-500"
                  }
                >
                  Mật Khẩu
                </label>
                {errors2.password && (
                  <p className="pl-2 mt-1 text-xs italic text-red-500">
                    Mật khẩu không được để trống
                  </p>
                )}
              </div>
              <div className="relative flex flex-col mt-7">
                <input
                  type="password"
                  className={
                    errors2.repassword
                      ? "input-login form-input border border-red-500"
                      : "input-login form-input border border-tertiary-500"
                  }
                  placeholder=" "
                  {...register2("repassword", { required: true })}
                />
                <label
                  htmlFor="confirm-password"
                  className={
                    errors2.repassword
                      ? "label-login form-label text-red-500"
                      : "label-login form-label text-tertiary-500"
                  }
                >
                  Nhập Lại Mật Khẩu
                </label>
                {errors2.repassword && (
                  <p className="pl-2 mt-1 text-xs italic text-red-500">
                    Mật khẩu không được để trống
                  </p>
                )}
              </div>
              <div
                className={`flex items-center mt-4 text-xs ${ringAnimation}`}
              >
                <input
                  id="checkbox"
                  type="checkbox"
                  className={`mr-2 outline-none border-secondary-500`}
                  onChange={handleChangeAccept}
                />
                <label htmlFor="checkbox" className="text-xs">
                  Tôi đã đồng ý với{" "}
                  <a href="" className="text-primary font-semibold">
                    Các Điều Khoản
                  </a>{" "}
                  và{" "}
                  <a href="" className="text-primary font-semibold">
                    Chính Sách Bảo Mật
                  </a>
                </label>
              </div>
              <div className="flex justify-between w-full mt-8 gap-x-9">
                <button
                  onClick={returnLastPage}
                  className="w-1/2 p-2 text-center text-primary cursor-pointer border border-primary bg-transparent rounded-xl"
                >
                  Quay lại
                </button>
                {isLoading ? (
                  <div className="flex items-center justify-center w-1/2 p-2 bg-primary rounded-xl">
                    <SpinnerLoader />
                  </div>
                ) : (
                  <input
                    type="submit"
                    className="w-1/2 p-2 text-center text-white cursor-pointer bg-primary rounded-xl"
                    value="Đăng ký"
                  />
                )}
              </div>
            </form>
            <div className="mt-5 text-sm text-center">
              <span>
                Đã có tài khoản?{" "}
                <Link href="/login">
                  <a className="text-primary font-semibold">Đăng nhập</a>
                </Link>
              </span>
            </div>
          </div>
        </div>
        <div className="justify-center hidden w-1/2 md:flex">
          <Image
            src="/image/login/signup-1.jpg"
            alt="signup"
            width={600}
            height={600}
            objectFit="cover"
          />
        </div>
      </section>
    </motion.div>
  );
};

const Register = () => {
  const [currentView, setCurrentView] = useState("register");
  const [user, setUser] = useState<RegisterPayload>({
    hoten: "",
    email: "",
    sdt: "",
    address: "",
    password: "",
  });
  function handleChangeView(view: string) {
    setCurrentView(view);
  }

  return (
    <>
      {currentView === "register" && (
        <RegisterView
          handleChangeView={handleChangeView}
          user={user}
          setUser={setUser}
        />
      )}
      {currentView === "register2" && (
        <RegisterView2
          handleChangeView={handleChangeView}
          user={user}
          setUser={setUser}
        />
      )}
    </>
  );
};

Register.Layout = LoginLayout;

export default Register;
