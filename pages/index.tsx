import type { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import { motion } from "framer-motion";
import animations from "../constants/animation";
import dynamic from "next/dynamic";

const SliderProjectItems = dynamic(
  () => import("../components/Slider/SliderProjectItems"),
  {
    ssr: false,
  }
);

var projectList = [
  {
    id: 1,
    image: "/image/logo-we/name-6.png",
    width: 300,
    height: 90,
    desc: "Phát triển du lịch - trải nghiệm cộng đồng là chương trình được thành lập với mục đích tạo thêm công ăn việc làm, thúc đẩy cơ sở hạ tầng du lịch tại các địa phương chưa được chú ý nhiều về mặt du lịch; giúp du khách có những trải nghiệm mới tại những địa điểm kỳ thú, đồng thời mang lại nguồn thu nhập cho những người dân địa phương.",
  },
  {
    id: 2,
    image: "/image/logo-we/name-4.png",
    width: 300,
    height: 90,
    desc: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
  },
  {
    id: 3,
    image: "/image/logo-we/name-2.png",
    width: 200,
    height: 100,
    desc: "Hoạt động Suối hồng ấm nồng yêu thương là nơi kết nối những cá nhân, tập thể, đơn vị hảo tâm đến với các hoàn cảnh khó khăn vươn lên trong cuộc sống, góp phần lan tỏa tình yêu thương nhân ái trong mỗi cá nhân, mỗi gia đình Việt và xây dựng một xã hội tốt đẹp giàu tính nhân văn.",
  },
  {
    id: 4,
    image: "/image/logo-we/name-5.png",
    width: 300,
    height: 150,
    desc: "Phát huy truyền thống đạo lý  “Uống nước nhớ nguồn - Ăn quả nhớ người trồng cây” của các tầng lớp nhân dân đối với thế hệ trước đã hy sinh vì sự nghiệp xây dựng và bảo vệ Tổ quốc Việt Nam thân yêu.",
  },
  {
    id: 5,
    image: "/image/logo-we/name-3.png",
    width: 250,
    height: 150,
    desc: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
  },
  {
    id: 6,
    image: "/image/logo-we/name-1.png",
    width: 320,
    height: 150,
    desc: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
  },
];

const Home: NextPage = () => {
  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="my-24"
    >
      <div className="bg-no-repeat bg-contain bg-line-4">
        <div className="py-20">
          <div className="flex flex-col items-center justify-center gap-8 mx-auto max-w-screen-xxl">
            <h2 className="text-4xl font-semibold text-center">W.E Quỹ</h2>
            <p className="w-4/6 text-center text-lg">
              Quỹ từ thiện WE - viết tắt của Warm Embrace - là quỹ từ thiện được
              thành lập và có pháp nhân chính thức là Chương trình Công tác xã
              hội Anh hùng lực lượng vũ trang Phan Trọng Bình. WE được thành lập
              với mục đích hoạt động phi lợi nhuận, là cầu nối giúp khắc phục
              các sự cố thiên tai, lũ lụt, dịch bệnh; đồng thời hỗ trợ các cá
              nhân yếu thế trong cộng đồng.
            </p>
            <Link href="/projects">
              <a className="block px-4 py-2 text-white shadow-lg bg-primary rounded-xl ">
                Tìm hiểu thêm
              </a>
            </Link>
          </div>
        </div>
        <div className="flex items-center justify-center px-10 mx-auto -mb-80 md:-mb-96 md:px-0 max-w-screen-xxl">
          <div className="relative flex gap-5">
            <motion.div
              className="flex items-center w-full -mt-10 -mr-10 overflow-hidden shadow-xl h-fit rounded-xl"
              initial={{ opacity: 0, y: -100 }}
              animate={{
                opacity: 1,
                y: 0,
                transition: { delay: 0.5, type: "spring" },
              }}
            >
              <Image
                src="/image/devices/screen-2.jpg"
                className="rounded-xl"
                width={300}
                height={500}
                alt="devices"
              />
            </motion.div>
            <motion.div
              className="relative z-10 flex items-center w-full overflow-hidden shadow-xl h-fit rounded-xl"
              initial={{ opacity: 0, y: -100 }}
              animate={{
                opacity: 1,
                y: 0,
                transition: { delay: 0.8, type: "spring" },
              }}
            >
              <Image
                src="/image/devices/screen-2.jpg"
                className="rounded-xl"
                width={300}
                height={500}
                alt="devices"
              />
            </motion.div>
            <motion.div
              className="flex items-center w-full -mt-10 -ml-10 overflow-hidden shadow-xl h-fit rounded-xl"
              initial={{ opacity: 0, y: -100 }}
              animate={{
                opacity: 1,
                y: 0,
                transition: { delay: 1.1, type: "spring" },
              }}
            >
              <Image
                src="/image/devices/screen-2.jpg"
                className="rounded-xl"
                width={300}
                height={500}
                alt="devices"
              />
            </motion.div>
          </div>
        </div>
        <div className="w-full px-16 mt-32 bg-no-repeat bg-cover bg-project pb-14 pt-72 md:pt-96">
          <h2 className="mb-20 text-4xl font-semibold text-center">
            Các Chủ Đề Thiện Nguyện
          </h2>
          <div className="grid items-start w-full grid-cols-1 gap-y-10 gap-x-12 md:grid-cols-3 ">
            {projectList.map((item, idx) => {
              return (
                <motion.div
                  className="flex flex-col items-center justify-center md:space-y-5"
                  key={idx}
                  initial={{ opacity: 0, translateY: -50 }}
                  animate={{
                    opacity: 1,
                    translateY: 0,
                  }}
                  transition={{
                    duration: 0.4,
                    delay: idx * 0.4 > 1 ? idx * 0.2 : idx * 0.4,
                  }}
                  whileInView={{
                    opacity: 1,
                    translateY: 0,
                  }}
                >
                  <Image
                    src={item.image}
                    width={item.width}
                    height={item.height}
                    alt="project-item"
                  />
                  <p className="hidden w-full text-center md:block">
                    {item.desc}
                  </p>
                </motion.div>
              );
            })}
          </div>
        </div>
        <div className="px-5 py-10 bg-no-repeat md:px-16 bg-line-2">
          <div className="mx-auto max-w-screen-xxl">
            <h3 className="mb-16 text-4xl font-semibold text-center">
              {" "}
              Lorem Ipsum
            </h3>
            <SliderProjectItems projects={[]} />
          </div>
        </div>
        <div className="flex">
          <button className="px-4 py-2 mx-auto text-white shadow-lg bg-primary rounded-xl">
            Xem thêm
          </button>
        </div>
      </div>
    </motion.div>
  );
};

export default Home;
