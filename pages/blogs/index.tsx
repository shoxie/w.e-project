import BlogLayout from "../../components/Layout/Blog";
import { motion } from "framer-motion";
import animations from "../../constants/animation";

const BlogPage = () => {
  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className=""
    >
      <div className="md:px-0 md:mt-0">
        <div className="py-20 px-0 md:px-16 bg-project bg-no-repeat bg-cover min-h-screen">
          <div className="flex flex-col items-center justify-center gap-8 mx-auto max-w-screen-xxl">
            <h2 className="text-4xl font-semibold text-center">Coming soon</h2>
            <p className="w-4/6 text-center">
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout. The
              point of using Lorem Ipsum is that it has a more-or-less normal
              distribution of letters, as opposed to using Content here, content
              here, making it look like readable English. Many desktop
              publishing packages and web page editors now use Lorem Ipsum as
              their default model text, and a search for lorem ipsum will
              uncover many web sites still in their infancy. Various versions
              have evolved over the years, sometimes by accident, sometimes on
              purpose (injected humour and the like).
            </p>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

BlogPage.Layout = BlogLayout;

export default BlogPage;
