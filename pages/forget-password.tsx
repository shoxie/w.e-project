import Image from "next/image";
import { useState } from "react";
import * as React from "react";
import { motion } from "framer-motion";
import animations from "../constants/animation";
import Link from "next/link";
import LoginLayout from "../components/Layout/Login";
import { useForm, SubmitHandler } from "react-hook-form";
import { useDispatch } from "react-redux";
import { forgetPassword } from "../app/redux/authSlice";
import { ForgetPasswordPayload } from "../constants/request.spec";

interface PropsType {
  handleChangeView: (view: string) => void;
}

const ForgetPassword = (props: PropsType) => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ForgetPasswordPayload>();
  const onSubmit: SubmitHandler<ForgetPasswordPayload> = (data) => {
    //send api
    dispatch(forgetPassword({ email: data.email }));
  };

  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="relative flex items-center w-full min-h-screen"
    >
      <section className="flex items-center justify-center w-full gap-1 py-4 mt-20 md:px-16 xxl:mt-0">
        <div className="flex justify-center w-full bg-cover bg-line-1">
          <div className="w-10/12 px-8 py-16 bg-white border rounded-md border-tertiary-500 md:w-5/12">
            <h3 className="text-2xl font-semibold text-center">
              KHÔI PHỤC TÀI KHOẢN
            </h3>
            <div className="flex justify-center w-full mt-3 text-center">
              <span className="block text-xs md:w-3/4 text-tertiary-600">
                Nhập địa chỉ email bạn đã đăng ký vào ô bên dưới để khôi phục
                tài khoản
              </span>
            </div>
            <form className="mt-6" onSubmit={handleSubmit(onSubmit)}>
              <div className="relative flex flex-col">
                <input
                  className={
                    errors.email
                      ? "input-login form-input border border-red-500"
                      : "input-login form-input border border-tertiary-500"
                  }
                  placeholder=" "
                  {...register("email", { required: true })}
                />
                <label
                  htmlFor="email"
                  className={
                    errors.email
                      ? "label-login form-label text-red-500"
                      : "label-login form-label text-tertiary-500"
                  }
                >
                  Email
                </label>
                {errors.email && (
                  <p className="pl-2 mt-1 text-xs italic text-red-500">
                    Email không được để trống
                  </p>
                )}
              </div>
              <div className="w-full p-2 mt-8 text-center text-white bg-primary rounded-xl ">
                <input
                  type="submit"
                  className="w-full cursor-pointer text-tertiary-200"
                  value="Tiếp tục"
                />
              </div>
            </form>
          </div>
        </div>
      </section>
    </motion.div>
  );
};

const ForgetPassword2 = (props: PropsType) => {
  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="relative flex items-center w-full min-h-screen"
    >
      <section className="flex items-center justify-center w-full gap-1 py-4 mt-20 md:px-16 xxl:mt-0">
        <div className="flex justify-center w-full bg-cover bg-line-2">
          <div className="w-10/12 px-8 py-16 bg-white border rounded-md border-tertiary-500 md:w-5/12">
            <h3 className="text-2xl font-semibold text-center">
              KHÔI PHỤC TÀI KHOẢN
            </h3>
            <div className="flex justify-center w-full mt-3 text-center">
              <span className="block w-full text-xs md:w-3/4 text-tertiary-600">
                Chúng tôi đã gửi đến email của bạn đường link khôi phục tài
                khoản. Kiểm tra mail để biết thêm thông tin chi tiết
              </span>
            </div>
            <div className="w-full mt-6 text-center">
              <Image
                src="/image/icon/icon-email.jpg"
                alt="icon-email"
                width={60}
                height={60}
              />
            </div>
            <form className="mt-6">
              <div className="w-full p-2 mt-8 text-center text-white bg-primary rounded-xl">
                <Link href="/">
                  <a className="w-full cursor-pointer">Trở về trang chủ</a>
                </Link>
              </div>
            </form>
          </div>
        </div>
      </section>
    </motion.div>
  );
};

const Login = () => {
  const [currentView, setCurrentView] = useState("forget");

  function handleChangeView(view: string) {
    setCurrentView(view);
  }
  return (
    <>
      {currentView === "forget" && (
        <ForgetPassword handleChangeView={handleChangeView} />
      )}
      {currentView === "forget2" && (
        <ForgetPassword2 handleChangeView={handleChangeView} />
      )}
    </>
  );
};

Login.Layout = LoginLayout;

export default Login;
