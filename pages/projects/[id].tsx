import { motion } from "framer-motion";
import animations from "../../constants/animation";
import ProjectLayout from "../../components/Layout/Project";
import { BsStarFill } from "react-icons/bs";
import { IoMdFlower } from "react-icons/io";
import Image from "next/image";
import Link from "next/link";
import dynamic from "next/dynamic";
import { GetServerSideProps } from "next";
import { IRootState } from "../../app/redux/store";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { getOneProject } from "../../app/redux/projectSlice";
// @ts-ignore
import edjsParser from "editorjs-parser";

const SliderProjectInfo = dynamic(
  () => import("../../components/Slider/SliderProjectInfo"),
  {
    ssr: false,
  }
);

function Progress() {
  return (
    <div className="relative w-full h-2 mt-5 rounded-full bg-secondary-400">
      <div className="absolute top-0 left-0 w-1/2 h-full rounded-full bg-primary group">
        <div className="flex justify-end mt-3 transition-all md:opacity-0 md:group-hover:opacity-100">
          <span className="p-1 text-xs text-white rounded-md bg-primary">
            40.000.000
          </span>
        </div>
      </div>
      <BsStarFill className="absolute -top-[0.35rem] left-1/3 text-xl text-yellow-300" />
      <BsStarFill className="absolute text-xl -top-[0.35rem] left-2/3" />
      <IoMdFlower className="absolute text-3xl -top-3 -right-2" />
    </div>
  );
}

const ProjectDetail = ({ id }: { id: number }) => {
  const [post, setPost] = useState<any>();
  const currentProject = useSelector(
    (state: IRootState) => state.project.currentProject
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOneProject(id));
  }, [id, dispatch]);

  // function customParser(block:any){
  //   return `<table> ${block.data.text} </table>`;
  // }

  const customeParser = {
    image: (data: any, config: any) => {
      return `
      <div class="w-full flex flex-col justify-center items-center space-y-5">
        <img src="${data.file.url}" alt="${data.file.alt}" class="rounded-xl w-full md:w-3/4">
        <div class="w-full text-center italic text-md">${data.caption}</div>
      </div>
      `;
    },
    header: (data: any, config: any) => {
      return `
      <h1  class="text-4xl font-semibold text-center">${data.text}</h1>
      `;
    },
    paragraph: (data: any) => {
      return `
      <p class="my-5 text-lg text-center" style="text-align:${data.alignment} ">${data.text}</p>
      `;
    },
    table: (data: any) => {
      console.log("data ne", data);
      return `
      <div class="my-5" style="text-align:${data.alignment} "></div>
      `;
    },
    list: (data: any) => {
      return `
      <div class="my-5 " style="text-align:${data.alignment} ">${data.text}</div>
      `;
    },
    // raw: (data:any) => {
    //   const customStyleForIframe = ` style="width:100%;height:450px" `;
    //   let newHTML;
    //   if (data.html.includes("<iframe")) {
    //     const indexToAdd = data.html.indexOf("<iframe ") + 8;
    //     newHTML = addStr(data.html, indexToAdd, customStyleForIframe);
    //   } else {
    //     newHTML = data.html;
    //   }
    //   return `
    //   <div class="max-w-full overflow-y-scroll">${newHTML}</div>
    //   `;
    // },
    // quote: (data:any, string:any) => {
    //   return `
    //   <blockquote class="${customBlockStyle.customBlockquote}">
    //     <div class="${customBlockStyle.customBlockquote__wrapper}">
    //       <i class="bx bxs-quote-alt-left"></i>
    //       <p>${data.text}</p>
    //     </div>
    //     </blockquote>
    //   `;
    // },
  };

  useEffect(() => {
    if (currentProject) {
      if (currentProject.noiDung) {
        const parser = new edjsParser(undefined, customeParser, undefined);
        var html = parser.parse(JSON.parse(currentProject.noiDung));
        setPost(html);
      }
    }
  }, [currentProject]);

  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="bg-no-repeat bg-line-3"
    >
      <div className="flex flex-col items-center px-10 mx-auto md:px-16 max-w-screen-xxl">
        {/*profile cover*/}
        <div className="relative w-full overflow-hidden rounded-xl h-72 md:shadow-xl md:px-0">
          <img
            src={currentProject?.thumbnail ?? "https://picsum.photos/1920/1080"}
            className="object-cover w-full h-full cursor-pointer rounded-xl"
            alt="profile-cover"
          />
        </div>
        <div className="flex flex-col w-full px-10 -mt-16 items-flex-start md:-mt-36">
          <div className="flex flex-col items-center pt-5 md:justify-start md:w-auto md:pt-0">
            {/*user profile picture */}
            <div className="relative flex items-center justify-center">
              <img
                src={
                  currentProject?.thumbnail ?? "https://picsum.photos/1920/1080"
                }
                className="w-32 h-32 border-4 border-white rounded-full md:w-64 md:h-64"
                alt="profile-picture"
              />
            </div>
            {/*user name and points */}
            <div className="mt-4">
              <span className="text-4xl font-bold text-center">
                {currentProject?.name}
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="grid items-start w-full grid-cols-1 gap-10 mx-auto mt-16 md:grid-cols-2 md:px-16 max-w-screen-xxl">
        <div className="flex flex-col items-center justify-center">
          <div className="flex items-center justify-center w-3/4 py-2 mb-10 rounded-lg shadow-lg bg-secondary-100">
            <Image
              src="/image/logo-we/name-4.png"
              width={200}
              height={70}
              alt="project-we"
            />
          </div>
          <div className="w-3/4 px-10 py-4 rounded-lg shadow-lg cursor-pointer bg-secondary-100">
            <h4 className="mb-5 text-xl font-semibold">
              Danh sách những người đóng góp mới nhất
            </h4>
            <div className="font-semibold">
              {currentProject?.moneyDonation?.map((item, idx) => {
                return (
                  <div key={idx} className="mt-2">
                    {idx}. {item.money_donators.hoten}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
        <div className="w-full px-10">
          <div className="w-full px-10 py-8 shadow-lg bg-secondary-100 rounded-xl">
            <h3 className="text-xl font-semibold text-center">
              Thông tin chi tiết dự án
            </h3>
            <Progress />
            {/* <h5 className="mt-10">Lorem Ipsum:</h5> */}
            <div className="mt-5 text-justify text-md">
              <div className="mb-5">
                <span className="font-md text-2xl">Thông tin pháp nhân</span>
              </div>
              <div className="flex flex-row justify-between items-center">
                <div>
                  <span>Tên chủ dự án</span>
                </div>
                <div>
                  <span>{currentProject?.projectOwnerName}</span>
                </div>
              </div>
              <div className="flex flex-row justify-between items-center">
                <div>
                  <span>Email chủ dự án</span>
                </div>
                <div>
                  <span>{currentProject?.projectOwnerEmail}</span>
                </div>
              </div>
              <div className="flex flex-row justify-between items-center">
                <div>
                  <span>Số điện thoại</span>
                </div>
                <div>
                  <span>{currentProject?.projectOwnerPhone}</span>
                </div>
              </div>
              <div className="my-5">
                <span className="font-md text-2xl">Mô tả dự án</span>
              </div>
              <div>
                <span>{currentProject?.description}</span>
              </div>
            </div>
            <div className="flex mt-10">
              <Link href="/donate">
                <a className="block px-4 py-2 mx-auto text-white shadow-lg bg-primary rounded-xl ">
                  Đóng góp
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="mt-16 bg-no-repeat bg-cover bg-project ">
        <div className="px-10 mx-auto py-14 md:px-16 max-w-screen-xxl">
          <div className="flex">
            <h3 className="mx-auto text-3xl font-semibold">Lorem Ipsum</h3>
          </div>
          <div className="mt-10">
            <div className="mt-10">
              <div className="flex flex-col gap-10 md:flex-row ">
                <div className="flex-1">
                  <Image
                    src="https://picsum.photos/1920/1080"
                    width={750}
                    height={500}
                    alt="project-image"
                    className="rounded-lg"
                  />
                </div>
                <div className="flex flex-col items-center justify-center flex-1 gap-5">
                  <p className="text-lg text-center">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu
                    elementum sed ullamcorper rutrum et convallis id sed. Massa
                    tortor blandit neque morbi erat egestas. Duis dictum nulla
                    viverra quis. Ipsum vehicula facilisis adipiscing faucibus
                    condimentum sem viverra.
                  </p>
                </div>
              </div>
            </div>
            <div className="block mt-10">
              <div className="flex flex-col-reverse gap-10 md:flex-row ">
                <div className="flex flex-col items-center justify-center flex-1 gap-5">
                  <p className="text-lg text-center">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu
                    elementum sed ullamcorper rutrum et convallis id sed. Massa
                    tortor blandit neque morbi erat egestas. Duis dictum nulla
                    viverra quis. Ipsum vehicula facilisis adipiscing faucibus
                    condimentum sem viverra.
                  </p>
                </div>
                <div className="flex-1">
                  <Image
                    src="https://picsum.photos/1920/1080"
                    width={750}
                    height={500}
                    alt="project-image"
                    className="rounded-lg"
                  />
                </div>
              </div>
            </div>
            <div className="mt-10">
              <div className="flex flex-col gap-10 md:flex-row ">
                <div className="flex-1">
                  <Image
                    src="https://picsum.photos/1920/1080"
                    width={750}
                    height={500}
                    alt="project-image"
                    className="rounded-lg"
                  />
                </div>
                <div className="flex flex-col items-center justify-center flex-1 gap-5">
                  <p className="text-lg text-center">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu
                    elementum sed ullamcorper rutrum et convallis id sed. Massa
                    tortor blandit neque morbi erat egestas. Duis dictum nulla
                    viverra quis. Ipsum vehicula facilisis adipiscing faucibus
                    condimentum sem viverra.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> */}
      <div
        dangerouslySetInnerHTML={{ __html: post }}
        className="max-w-screen-xl mx-auto py-20 px-10 md:px-16"
      ></div>
      <div className="mt-20 bg-line-1">
        <div className="mx-auto max-w-screen-xxl">
          <div className="flex">
            <h3 className="mx-auto text-3xl font-semibold">Lorem Ipsum</h3>
          </div>
          <div className="px-10 mt-10 md:px-16">
            <SliderProjectInfo />
          </div>
        </div>
      </div>
      <div className="flex mt-16 mb-32">
        <Link href="/donate">
          <a className="block px-4 py-2 mx-auto text-white shadow-lg bg-primary rounded-xl ">
            Đóng góp
          </a>
        </Link>
      </div>
    </motion.div>
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;
  if (id) {
    return {
      props: {
        id: parseInt(id as string),
      },
    };
  } else {
    return {
      notFound: true,
    };
  }
};

ProjectDetail.Layout = ProjectLayout;

export default ProjectDetail;
