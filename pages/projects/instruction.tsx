import { motion } from "framer-motion";
import animations from "../../constants/animation";
import ProjectLayout from "../../components/Layout/Project";
import { IoIosArrowForward } from "react-icons/io";
import { useState } from "react";

const InstructionPage = () => {
  const [show, setShow] = useState("top");
  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className="px-0 md:px-16 bg-line-3"
    >
      <div className="grid items-start w-full grid-cols-1 px-5 py-24 space-y-10 md:grid-cols-2 md:space-x-10 md:space-y-0 md:px-0 mx-auto max-w-screen-xxl">
        <motion.div
          initial={{ opacity: 0, translateY: -50 }}
          animate={{
            opacity: 1,
            translateY: 0,
          }}
          transition={{ duration: 0.4, delay: 0.5 }}
          whileInView={{
            opacity: 1,
            translateY: 0,
          }}
          className="px-5 md:px-0 w-full"
        >
          <div className="bg-white shadow-lg w-fit rounded-lg cursor-pointer">
            <div
              className={`flex items-center gap-x-4 px-10 py-4 ${
                show === "top" ? "text-primary bg-tertiary-700" : ""
              } text-lg font-semibold rounded-t-lg transition-all duration-200`}
              onClick={() => setShow("top")}
            >
              <h4>1. Hướng dẫn đóng góp hiện kim</h4>
              {show === "top" && <IoIosArrowForward />}
            </div>
            <div
              className={`flex items-center gap-x-4 px-10 py-4 ${
                show === "down" ? "text-primary bg-tertiary-700" : ""
              } text-lg font-semibold rounded-b-lg transition-all duration-200`}
              onClick={() => setShow("down")}
            >
              <h4>2. Hướng dẫn đóng góp hiện vật</h4>
              {show === "down" && <IoIosArrowForward />}
            </div>
          </div>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: -50 }}
          animate={{
            opacity: 1,
            translateY: 0,
          }}
          transition={{ duration: 0.4, delay: 0.8 }}
          whileInView={{
            opacity: 1,
            translateY: 0,
          }}
          className={`px-5 md:px-0 w-full ${
            show === "top" ? "block" : "hidden"
          }`}
        >
          <div className="p-10 bg-white shadow-lg rounded-xl">
            <h3 className="text-2xl font-semibold">
              1. Hướng Dẫn Đóng Góp Hiện Kim
            </h3>
            <div className="mt-5 text-justify text-md">
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout. The
              point of using Lorem Ipsum is that it has a more-or-less normal
              distribution of letters, as opposed to using Content here, content
              here, making it look like readable English. Many desktop
              publishing packages and web page editors now use Lorem Ipsum as
              their default model text, and a search for lorem ipsum will
              uncover many web sites still in their infancy. Various versions
              have evolved over the years, sometimes by accident, sometimes on
              purpose (injected humour and the like).
            </div>
          </div>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, translateY: -50 }}
          animate={{
            opacity: 1,
            translateY: 0,
          }}
          transition={{ duration: 0.4, delay: 0.8 }}
          whileInView={{
            opacity: 1,
            translateY: 0,
          }}
          className={`px-5 md:px-0 w-full ${
            show === "down" ? "block" : "hidden"
          }`}
        >
          <div className="p-10 bg-white shadow-lg rounded-xl">
            <h3 className="text-2xl font-semibold">
              2. Hướng Dẫn Đóng Góp Hiện Vật
            </h3>
            <div className="mt-5 text-justify text-md">
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout. The
              point of using Lorem Ipsum is that it has a more-or-less normal
              distribution of letters, as opposed to using Content here, content
              here, making it look like readable English. Many desktop
              publishing packages and web page editors now use Lorem Ipsum as
              their default model text, and a search for lorem ipsum will
              uncover many web sites still in their infancy. Various versions
              have evolved over the years, sometimes by accident, sometimes on
              purpose (injected humour and the like).
            </div>
          </div>
        </motion.div>
      </div>
    </motion.div>
  );
};

InstructionPage.Layout = ProjectLayout;

export default InstructionPage;
