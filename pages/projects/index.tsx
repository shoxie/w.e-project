import { useState, useEffect, Fragment } from "react";
import Image from "next/image";
import Link from "next/link";
import ProjectLayout from "../../components/Layout/Project";
import dynamic from "next/dynamic";
import { IoIosArrowDown } from "react-icons/io";
import { useDispatch, useSelector } from "react-redux";
import { getAllProjects } from "../../app/redux/projectSlice";
import { IRootState } from "../../app/redux/store";
import { Listbox, Transition } from "@headlessui/react";
import { FiChevronDown } from "react-icons/fi";
import { motion } from "framer-motion";
import animations from "../../constants/animation";
import { GetServerSideProps } from "next";
import { getAllTopics } from "../../app/redux/topicSlice";

const SliderProjectItems = dynamic(
  () => import("../../components/Slider/SliderProjectItems"),
  {
    ssr: false,
  }
);
const SliderProjectLanding = dynamic(
  () => import("../../components/Slider/SliderProjectLanding"),
  {
    ssr: false,
  }
);
const ScrollDown = dynamic(() => import("../../components/Icon/ScrollDown"), {
  ssr: false,
});

var projectList = [
  {
    id: 1,
    image: "/image/logo-we/name-6.png",
    width: 300,
    height: 90,
    topicId: null,
  },
  {
    id: 2,
    image: "/image/logo-we/name-4.png",
    width: 300,
    height: 90,
    topicId: null,
  },
  {
    id: 3,
    image: "/image/logo-we/name-2.png",
    width: 200,
    height: 100,
  },
  {
    id: 4,
    image: "/image/logo-we/name-5.png",
    width: 200,
    height: 100,
    topicId: null,
  },
  {
    id: 5,
    image: "/image/logo-we/name-3.png",
    width: 140,
    height: 100,
    topicId: null,
  },
  {
    id: 6,
    image: "/image/logo-we/name-1.png",
    width: 200,
    height: 100,
    topicId: null,
  },
];

const ProjectPage = ({ filter }: { filter: string }) => {
  const dispatch = useDispatch();
  const projectData = useSelector((state: IRootState) => state.project.list);
  const topicData = useSelector((state: IRootState) => state.topic.list);
  const [postQuantity, setPostQuantity] = useState<number>(3);
  const [topicId, setTopicId] = useState<number | null>(null);

  useEffect(() => {
    dispatch(getAllProjects(topicId));
    dispatch(getAllTopics());
  }, [dispatch, topicId]);

  useEffect(() => {
    dispatch(getAllProjects(topicId));
  }, [topicId, dispatch]);

  useEffect(() => {}, [projectData]);

  return (
    <motion.div
      variants={animations.pageTransition}
      initial="initial"
      animate="animate"
      exit="exit"
      className=""
    >
      <div className="-mt-3 md:px-0 md:mt-0">
        <div className="p-20 px-0 mb-32 md:px-16 bg-project ">
          <div className="flex flex-col items-center justify-center gap-8 mx-auto max-w-screen-xxl">
            <h2 className="text-4xl font-semibold text-center">Quỹ W.E</h2>
            <p className="w-4/6 text-center">
              Quỹ từ thiện WE - viết tắt của Warm Embrace - là quỹ từ thiện được
              thành lập và có pháp nhân chính thức là Chương trình Công tác xã
              hội Anh hùng lực lượng vũ trang Phan Trọng Bình. WE được thành lập
              với mục đích hoạt động phi lợi nhuận, là cầu nối giúp khắc phục
              các sự cố thiên tai, lũ lụt , dịch bệnh; đồng thời hỗ trợ các cá
              nhân yếu thế trong cộng đồng.
            </p>
            <ScrollDown />
          </div>
        </div>
        <div className="grid grid-cols-1 grid-rows-2 gap-10 px-8 mx-auto md:grid-cols-3 md:px-16 max-w-screen-xxl">
          {topicData.map((item, idx) => {
            return (
              <button
                onClick={() => setTopicId(item.topicId)}
                key={item.topicId}
                className="flex flex-col items-center justify-center w-full p-5 bg-white rounded-lg shadow-lg"
              >
                <div className="flex items-center justify-center h-40">
                  <Image
                    src={item.logo}
                    height="90rem"
                    width="200rem"
                    alt={item.title}
                  />
                </div>
                <p>{item.description}</p>
              </button>
            );
          })}
        </div>
        <div className="px-8 py-6 mt-10 border-t md:px-16 bg-line-2">
          <div className="mx-auto max-w-screen-xxl">
            <div className="flex items-center gap-5 flex-wrap">
              <div>Bộ lọc:</div>
              <div className="flex items-center gap-3">
                <select
                  id="select-project"
                  className="px-3 py-1 border rounded-lg outline-none appearance-none bg-secondary-100 max-w-[250px]"
                  onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
                    setTopicId(parseInt(e.target.value))
                  }
                >
                  {topicData.map((item, idx) => (
                    <option
                      key={idx}
                      value={item.topicId}
                      className="text-center text-white border outline-none appearance-none bg-secondary-200"
                    >
                      {item.title}
                    </option>
                  ))}
                </select>
                <IoIosArrowDown className="cursor-pointer" />
              </div>
            </div>
            <div className="mt-10">
              <h3 className="mb-10 text-3xl font-semibold text-center">
                Các Dự Án Thiện Nguyện
              </h3>
              <div className="">
                <SliderProjectItems projects={projectData} />
              </div>
            </div>
          </div>
        </div>
        <div className="px-8 py-10 mt-10 md:px-16 bg-secondary-300">
          <div className="mx-auto max-w-screen-xxl">
            <h3 className="text-3xl font-semibold text-center text-white">
              Các Hình Ảnh Gây Quỹ
            </h3>
            <SliderProjectLanding projects={projectData} />
          </div>
          <div className="flex items-center justify-end gap-5 mt-5">
            <div>View:</div>
            <div className="flex items-center gap-3 pr-3 rounded-lg">
              {/* <select
              id="select-project"
              className="px-2 py-1 text-center border rounded-l-lg outline-none appearance-none bg-secondary-100"
            >
              <option className="border outline-none bg-tertiary-700 text-primary">
                3
              </option>
              <option className="bg-white border outline-none">5</option>
              <option className="bg-white border outline-none">10</option>
            </select>
            <IoIosArrowDown /> */}
              <Listbox value={postQuantity} onChange={setPostQuantity}>
                <div className="relative mt-1">
                  <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-indigo-500 sm:text-sm">
                    <span className="block truncate">{postQuantity}</span>
                    <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                      <FiChevronDown
                        className="w-5 h-5 text-gray-400"
                        aria-hidden="true"
                      />
                    </span>
                  </Listbox.Button>
                  <Transition
                    as={Fragment}
                    leave="transition ease-in duration-100"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                  >
                    <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                      <Listbox.Option value={3} className="p-2 text-center">
                        <span>3</span>
                      </Listbox.Option>
                      <Listbox.Option value={5} className="p-2 text-center">
                        <span>5</span>
                      </Listbox.Option>
                      <Listbox.Option value={10} className="p-2 text-center">
                        <span>10</span>
                      </Listbox.Option>
                    </Listbox.Options>
                  </Transition>
                </div>
              </Listbox>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

ProjectPage.Layout = ProjectLayout;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { filter } = context.query;
  return {
    props: {
      filter: filter ? filter : "",
    },
  };
};

export default ProjectPage;
